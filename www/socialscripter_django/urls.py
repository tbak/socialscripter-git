from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
 
    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    
    # LOGIN/ETC
    (r'^login/$', 'django.contrib.auth.views.login', {'template_name':'main/login.html'} ),
    (r'^logout/$', 'django.contrib.auth.views.logout_then_login', {'login_url':'/'} ),    
    (r'^register/$', 'socialscripter_django.main.views.register' ),   
    
    # CAN BE SEEN BY ANYONE
    (r'^$', 'socialscripter_django.main.views.home'),
    (r'^browse/$', 'socialscripter_django.main.views.browse'),
    (r'^help/$', 'socialscripter_django.main.views.help'),
    (r'^privacy/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/privacy.html'}, 'privacy' ),
    (r'^contact/$', 'socialscripter_django.main.views.contact' ),
    (r'^contact_thankyou/$', 'socialscripter_django.main.views.contact_thankyou' ),    
       
    # REQUIRES AUTHENTICATION
    (r'^myscripts/$', 'socialscripter_django.main.views.user_scripts' ),      
    (r'^myhistory/$', 'socialscripter_django.main.views.user_history' ),
    
    (r'^script/new/$', 'socialscripter_django.main.views.new_script' ),
    (r'^script/copy/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.copy_script'),
    (r'^script/delete/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.delete_script'),
        
    (r'^script/edit/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.edit_script'),
    (r'^script/edit/(?P<script_id>\d+),(?P<open_tab>.+)/$', 'socialscripter_django.main.views.edit_script'),
    
    (r'^script/oauth/twitter/start/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.oauth_twitter_start'),
    (r'^script/oauth/twitter/success/$', 'socialscripter_django.main.views.oauth_twitter_success'),
    (r'^script/oauth/twitter/remove/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.oauth_twitter_remove'),
    
    (r'^script/auth/facebook/start/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.auth_facebook_start'),
    (r'^script/auth/facebook/success/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.auth_facebook_success'),
    (r'^script/auth/facebook/remove/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.auth_facebook_remove'),
    
    # POST / JSON URLS
    (r'^script/run/json/(?P<script_id>\d+)/$', 'socialscripter_django.main.views.run_script_json'),        
    (r'^script/save/json/$', 'socialscripter_django.main.views.save_script_json'),
    (r'^script/email/json/$', 'socialscripter_django.main.views.email_script_json'),
    (r'^script/post_twitter/json/$', 'socialscripter_django.main.views.post_twitter_json'),
    
    (r'^password_reset/$', 'django.contrib.auth.views.password_reset', {'template_name':'main/password_reset_form_new.html', 'email_template_name':'main/password_reset_email_new.html'}, 'password_reset'),
    (r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', {'template_name':'main/password_reset_done_new.html'}),
    (r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {'template_name':'main/password_reset_confirm_new.html'}, 'password_reset_confirm'),
    (r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', {'template_name':'main/password_reset_complete_new.html'}),
    
    (r'^captcha/', include('captcha.urls')),

        
)
