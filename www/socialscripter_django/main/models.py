from django.db import models
from django.contrib.auth.models import User

# I snagged this from http://www.smipple.net/snippet/IanLewis/Django%20Big%20Integer%20Fields
class BigIntegerField(models.IntegerField):
    empty_strings_allowed=False
  
    def get_internal_type(self):
        return "BigIntegerField"
  
    def db_type(self):
        return 'bigint' # Note this won't work with Oracle.    

class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)
    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)

class ScriptSocialTiming(models.Model):

    wakingtime_range_min = IntegerRangeField( min_value = 0, max_value = 5, default = 1 )
    wakingtime_range_max = IntegerRangeField( min_value = 0, max_value = 5, default = 2 )
    anytime_range_min = IntegerRangeField( min_value = 0, max_value = 5, default = 1 )
    anytime_range_max = IntegerRangeField( min_value = 0, max_value = 5, default = 2 )
    exacttime = models.TimeField( default="13:00" )
        
    FREQUENCY_CHOICES = (
        (0, 'Never'),
        (1, 'During normal waking hours'),
        (2, 'At any time of the day'),
        (3, 'At a specific time'),
        (4, 'As direct message to new followers (twitter only)'),
        (5, 'As direct message to @ reply (twitter only)'),
    )
    frequency = models.IntegerField(choices=FREQUENCY_CHOICES, default=0)
    
    def __unicode__(self):
        return str(self.frequency)

# By creating separate twitter and facebook account tables (instead of associating with the script),
# we can store a list of all accounts that a user has authenticated.
# This is necessary for facebook authentication since we only get one shot to store the session/uid info.
# After that, there appears to be no clean way to change the user or reauthenticate or whatnot.
# Now we just link each script to a TwitterAccount/FacebookAccount, which can be used by multpiple scripts.
class TwitterAccount(models.Model):
    user = models.ForeignKey(User, help_text="The owner of the account")
    oauth_access_token = models.CharField( max_length=255, blank=True )
    screen_name = models.CharField( max_length=100, blank=True )
    
    class Meta:
        unique_together = (("user", "screen_name"),)
        
    def __unicode__(self):
        return self.screen_name

class FacebookAccount(models.Model):
    user = models.ForeignKey(User, help_text="The owner of the account")
    uid = BigIntegerField( default = 0 )
    session_key = models.CharField( max_length=100, blank=True )
    secret = models.CharField( max_length=100, blank=True )
    screen_name = models.CharField( max_length=100, blank=True )
    
    class Meta:
        unique_together = (("user", "uid"),)
        
    def __unicode__(self):
        return self.screen_name


class Script(models.Model):
    
    user = models.ForeignKey(User, help_text="The owner of the script")    

    name = models.CharField( max_length=60 )
    description = models.TextField(blank=True)
    enabled = models.BooleanField(default=True)
    create_date = models.DateTimeField('date created', auto_now_add=True)
    modify_date = models.DateTimeField('date last modified', auto_now=True)
    status = models.IntegerField( default=0 )
    num_lines = models.IntegerField( default=0 )
        
    script_data = models.TextField(blank=True)        
    
    sharing_enabled = models.BooleanField(default=True)
    
    # When the scheduler last ran for this script.
    # It should run once a day...
    # This is local time, according to the time zone setting???
    # Or should it be UTC?
    # We just store the day. We update the script stuff just after midnight, local time.
    # So if the current local date is no longer the same as the last update date, we must update! 
    # scheduler_last_update_localtz = models.DateField( null=True )
    
    # This will specify midnight of the next day, timezone ajusted, represented as UTC
    # We want to update every script at midnight local time.
    # If we store it as utc instead of localtz, we can do a quicker select statement
    # in process_scripts.py instead of converting the tz for each enabled script. 
    scheduler_next_update_utc = models.DateTimeField( null=True )
    
    # First digit = negative if 1, positive if 2
    # Next 2 digits = hour
    # Next 2 digits = min
    # Final digit is to make values with the same time offset unique
    TIMEZONE_CHOICES = (
        (110000, '(GMT-10:00) Hawaii'),
        (109000, '(GMT-09:00) Alaska'),
        (108000, '(GMT-08:00) Pacific Time (US & Canada)'),
        (107000, '(GMT-07:00) Arizona'),
        (107000, '(GMT-07:00) Mountain Time (US & Canada)'),
        (106000, '(GMT-06:00) Central Time (US & Canada)'),
        (105000, '(GMT-05:00) Eastern Time (US & Canada)'),
        (105000, '(GMT-05:00) Indiana (East)'),
        (0, '-------------'),
        (111001, '(GMT-11:00) International Date Line West'),
        (111002, '(GMT-11:00) Midway Island'),
        (111003, '(GMT-11:00) Samoa'),
        (108001, '(GMT-08:00) Tijuana'),
        (107001, '(GMT-07:00) Chihuahua'),
        (107002, '(GMT-07:00) Mazatlan'),
        (106001, '(GMT-06:00) Central America'),
        (106002, '(GMT-06:00) Guadalajara'),
        (106003, '(GMT-06:00) Mexico City'),
        (106004, '(GMT-06:00) Monterrey'),
        (106005, '(GMT-06:00) Saskatchewan'),
        (105001, '(GMT-05:00) Bogota'),
        (105002, '(GMT-05:00) Lima'),
        (105003, '(GMT-05:00) Quito'),
        (104301, '(GMT-04:30) Caracas'),
        (104001, '(GMT-04:00) Atlantic Time (Canada)'),
        (104002, '(GMT-04:00) La Paz'),
        (104003, '(GMT-04:00) Santiago'),
        (103301, '(GMT-03:30) Newfoundland'),
        (103001, '(GMT-03:00) Brasilia'),
        (103002, '(GMT-03:00) Buenos Aires'),
        (103003, '(GMT-03:00) Georgetown'),
        (103004, '(GMT-03:00) Greenland'),
        (102001, '(GMT-02:00) Mid-Atlantic'),
        (101001, '(GMT-01:00) Azores'),
        (101002, '(GMT-01:00) Cape Verde Is.'),
        (200001, '(GMT) Casablanca'),
        (200002, '(GMT) Dublin'),
        (200003, '(GMT) Edinburgh'),
        (200004, '(GMT) Lisbon'),
        (200005, '(GMT) London'),
        (200006, '(GMT) Monrovia'),
        (201001, '(GMT+01:00) Amsterdam'),
        (201002, '(GMT+01:00) Berlin'),        
        (201003, '(GMT+01:00) Brussels'),
        (201004, '(GMT+01:00) Budapest'),
        (201005, '(GMT+01:00) Copenhagen'),
        (201006, '(GMT+01:00) Madrid'),
        (201007, '(GMT+01:00) Paris'),
        (201008, '(GMT+01:00) Rome'),
        (201009, '(GMT+01:00) West Central Africa'),
        (202001, '(GMT+02:00) Athens'),
        (202002, '(GMT+02:00) Bucharest'),
        (202003, '(GMT+02:00) Cairo'),
        (202004, '(GMT+02:00) Helsinki'),
        (202005, '(GMT+02:00) Istanbul'),
        (202006, '(GMT+02:00) Jerusalem'),
        (202007, '(GMT+02:00) Minsk'),
        (202008, '(GMT+02:00) Pretoria'),
        (202009, '(GMT+02:00) Tallinn'),
        (203001, '(GMT+03:00) Baghdad'),
        (203002, '(GMT+03:00) Kuwait'),
        (203003, '(GMT+03:00) Moscow'),
        (203004, '(GMT+03:00) Nairobi'),
        (203005, '(GMT+03:00) Riyadh'),
        (203006, '(GMT+03:00) St. Petersburg'),
        (203007, '(GMT+03:00) Volgograd'),
        (203301, '(GMT+03:30) Tehran'),
        (204001, '(GMT+04:00) Abu Dhabi'),
        (204002, '(GMT+04:00) Baku'),
        (204003, '(GMT+04:00) Muscat'),
        (204004, '(GMT+04:00) Tbilisi'),
        (204005, '(GMT+04:00) Yerevan'),
        (204301, '(GMT+04:30) Kabul'),
        (205001, '(GMT+05:00) Ekaterinburg'),
        (205002, '(GMT+05:00) Islamabad'),
        (205003, '(GMT+05:00) Karachi'),
        (205004, '(GMT+05:00) Tashkent'),
        (205301, '(GMT+05:30) Chennai'),
        (205302, '(GMT+05:30) Kolkata'),
        (205303, '(GMT+05:30) Mumbai'),
        (205304, '(GMT+05:30) New Delhi'),
        (205451, '(GMT+05:45) Kathmandu'),
        (206001, '(GMT+06:00) Almaty'),
        (206002, '(GMT+06:00) Astana'),
        (206003, '(GMT+06:00) Dhaka'),
        (206004, '(GMT+06:00) Novosibirsk'),
        (206005, '(GMT+06:00) Sri Jayawardenepura'),
        (206301, '(GMT+06:30) Rangoon'),
        (207001, '(GMT+07:00) Bangkok'),
        (207002, '(GMT+07:00) Hanoi'),
        (207003, '(GMT+07:00) Jakarta'),
        (207004, '(GMT+07:00) Krasnoyarsk'),
        (208001, '(GMT+08:00) Beijing'),
        (208002, '(GMT+08:00) Hong Kong'),
        (208003, '(GMT+08:00) Irkutsk'),
        (208004, '(GMT+08:00) Kuala Lumpur'),
        (208005, '(GMT+08:00) Perth'),
        (208006, '(GMT+08:00) Singapore'),
        (208007, '(GMT+08:00) Taipei'),
        (209001, '(GMT+09:00) Seoul'),
        (209002, '(GMT+09:00) Tokyo'),
        (209003, '(GMT+09:00) Yakutsk'),
        (209301, '(GMT+09:30) Adelaide'),
        (209302, '(GMT+09:30) Darwin'),
        (210001, '(GMT+10:00) Brisbane'),
        (210002, '(GMT+10:00) Canberra'),
        (210003, '(GMT+10:00) Guam'),
        (210004, '(GMT+10:00) Hobart'),
        (210005, '(GMT+10:00) Melbourne'),
        (210006, '(GMT+10:00) Port Moresby'),
        (210007, '(GMT+10:00) Sydney'),
        (210008, '(GMT+10:00) Vladivostok'),
        (211001, '(GMT+11:00) Magadan'),
        (211002, '(GMT+11:00) New Caledonia'),
        (211003, '(GMT+11:00) Solomon Is.'),
        (212001, '(GMT+12:00) Auckland'),
        (212002, '(GMT+12:00) Fiji'),
        (212003, '(GMT+12:00) Kamchatka'),
        (212004, '(GMT+12:00) Marshall Is.'),
        (212005, '(GMT+12:00) Wellington'),
        (213001, "(GMT+13:00) Nuku'alofa"),
                       
        
    )
    localtz = models.IntegerField(choices=TIMEZONE_CHOICES, default=108000)

    # But I need this to get the actual offset in seconds.
    localtz_utc_offset_secs = models.IntegerField( default=0 ) 
    
    # TWITTER
    # Moved to TwitterAccount    
    # twitter_oauth_access_token = models.CharField( max_length=255, blank=True )
    # twitter_username = models.CharField( max_length=100, blank=True )
    twitter_account = models.ForeignKey(TwitterAccount, null=True, blank=True, help_text="The Twitter account to associate with this script.")
    
    twitter_befriend_followers = models.BooleanField(default=True)
    twitter_befriend_replies = models.BooleanField(default=True)    
    twitter_timing = models.ForeignKey(ScriptSocialTiming, help_text="Twitter posting timing", related_name="twitter_timing")
    """
    twitter_wakingtime_range_min = IntegerRangeField( min_value = 0, max_value = 5, default = 1 )
    twitter_wakingtime_range_max = IntegerRangeField( min_value = 0, max_value = 5, default = 2 )
    twitter_anytime_range_min = IntegerRangeField( min_value = 0, max_value = 5, default = 1 )
    twitter_anytime_range_max = IntegerRangeField( min_value = 0, max_value = 5, default = 2 )
    twitter_exacttime = models.TimeField( default="13:00" )
        
    SOCIAL_FREQUENCY_CHOICES = (
        (0, 'Never'),
        (1, 'During normal waking hours'),
        (2, 'At any time of the day'),
        (3, 'At a specific time'),
        (4, 'As direct message to new followers (twitter only)'),
        (5, 'As direct message to @ reply (twitter only)'),
    )
    twitter_frequency = models.IntegerField(choices=SOCIAL_FREQUENCY_CHOICES, default=0)
    """
    
    twitter_num_posts = models.IntegerField(default=0)
    twitter_last_reply_id = BigIntegerField( default = 0 )

    # FACEBOOK
    # Moved to FacebookAccount
    # facebook_uid = BigIntegerField( default = 0 )
    # facebook_session_key = models.CharField( max_length=100, blank=True )
    # facebook_secret = models.CharField( max_length=100, blank=True )
    # facebook_name = models.CharField( max_length=100, blank=True )
    facebook_account = models.ForeignKey(FacebookAccount, null=True, blank=True, help_text="The Facebook account to associate with this script.")
    
    facebook_timing = models.ForeignKey(ScriptSocialTiming, help_text="Facebook posting timing", related_name="facebook_timing")
    """
    facebook_wakingtime_range_min = IntegerRangeField( min_value = 0, max_value = 5, default = 1 )
    facebook_wakingtime_range_max = IntegerRangeField( min_value = 0, max_value = 5, default = 2 )
    facebook_anytime_range_min = IntegerRangeField( min_value = 0, max_value = 5, default = 1 )
    facebook_anytime_range_max = IntegerRangeField( min_value = 0, max_value = 5, default = 2 )
    facebook_exacttime = models.TimeField( default="13:00" )    
    
    facebook_frequency = models.IntegerField(choices=SOCIAL_FREQUENCY_CHOICES, default=0)
    """
    
    facebook_num_posts = models.IntegerField(default=0)
    
    class Meta:        
        # unique_together = (("user", "name"),)
        ordering = ('name',)   

    def __unicode__(self):
        return self.name

class TwitterFollower(models.Model):
    """ This is some other dude that is following this script.
    This is used to keep track of what follower direct messages we've already posted (and who we've already followed)
    It can also be used with the system replacer to get a random follower user name.
    """
    script = models.ForeignKey(Script)
    follower_username = models.CharField( max_length=100, blank=True )
    
    class Meta:        
        unique_together = (("script", "follower_username"),)
        
class TwitterTrend(models.Model):
    enabled = models.BooleanField(default=True)
    trend = models.CharField( max_length=50, blank=True )
    
    def __unicode__(self):
        return self.trend        

class ScheduledPost(models.Model):
    script = models.ForeignKey(Script)    
        
    post_time_utc = models.DateTimeField()
    
    SOCIAL_SERVICE_CHOICES = (
        ('T', 'Twitter'),
        ('D', 'Twitter Direct Message'),
        ('F', 'Facebook'),
    )
    social_service = models.CharField(max_length=1, choices=SOCIAL_SERVICE_CHOICES)
    target_user = models.CharField(max_length=100, blank=True)
    
    # Has it been posted to twitter/facebook whatever?
    posted = models.BooleanField(default=False)
    
    # A post can be disabled and not posted if the user changes scheduling options,
    # causing a reschedule to take place.
    enabled = models.BooleanField(default=True)
    
    class Meta:        
        # unique_together = (("user", "name"),)
        ordering = ('post_time_utc',)   

    def __unicode__(self):
        return self.script.name
      
    
class History(models.Model):
    user = models.ForeignKey(User, help_text="Who this pertains to")
    script = models.ForeignKey(Script, help_text="The relevant script")
    create_date = models.DateTimeField('date created', auto_now_add=True)
    
    HISTORY_EVENT_CHOICES = (
        (0, 'Script created'),
        (1, 'Script deleted'),
        (100, 'Twitter post success'),
        (101, 'Twitter post error'),
        (102, 'Twitter direct message success'),        
        (103, 'Twitter direct message error'),
        (200, 'Facebook post success'),        
        (201, 'Facebook post error'),
    )
    event = models.IntegerField(choices=HISTORY_EVENT_CHOICES)
    
    # For twitter direct messages, this could hold the target user id???
    extra = models.CharField(max_length=50, blank=True, null=True) 
    
    class Meta:
        ordering = ('create_date',)   

    def __unicode__(self):
        return self.script.name