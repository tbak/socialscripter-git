# Copyright 2009, Tom Bak

from socialscripter_django.main.models import Script, ScheduledPost, History, TwitterFollower, TwitterTrend, ScriptSocialTiming
from django.contrib import admin 


class ScriptAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'create_date')
    list_filter = ['create_date']
    date_hierarchy = 'create_date'
    pass

class ScheduledPostAdmin(admin.ModelAdmin):
    list_display = ('enabled', 'posted', 'social_service', 'post_time_utc', 'script')
    pass

class HistoryAdmin(admin.ModelAdmin):
    list_display = ('user', 'script', 'create_date', 'event', 'extra')
    pass

class TwitterFollowerAdmin(admin.ModelAdmin):
    list_display = ('script', 'follower_username')
    pass

class TwitterTrendAdmin(admin.ModelAdmin):
    list_display = ('trend', 'enabled')
    pass

class ScriptSocialTimingAdmin(admin.ModelAdmin):    
    pass

admin.site.register(Script, ScriptAdmin)
admin.site.register(ScheduledPost, ScheduledPostAdmin)
admin.site.register(History, HistoryAdmin)
admin.site.register(TwitterFollower, TwitterFollowerAdmin)
admin.site.register(TwitterTrend, TwitterTrendAdmin)
admin.site.register(ScriptSocialTiming, ScriptSocialTimingAdmin)