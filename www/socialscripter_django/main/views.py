from socialscripter_django.main.models import Script, History, TwitterFollower, ScriptSocialTiming, TwitterAccount, FacebookAccount
from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template.context import RequestContext
from socialscripter_django.random_sentence.lex import Lex
from socialscripter_django.random_sentence import filters, replacers
from django.contrib.auth.models import User, Permission
from django.contrib.auth import authenticate, login, logout
from django.db.models.query import QuerySet
import logging
from django.conf import settings
#import json
from django.utils import simplejson
from django.utils import html
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from socialscripter_django.captcha.fields import CaptchaField
from django.core.mail import send_mail
import twitter
import facebook
import oauthtwitter
from urllib2 import HTTPError
import re
import inspect
import datetime
import time
import os
import copy
import socialscripter_django.main.kungfutimefield as kungfutimefield
from django.contrib.sites.models import Site
from django.contrib.auth.decorators import login_required


# These were obtained from http://twitter.com/oauth_clients/details/69104

#if os.environ['DJANGOTEST_SERVER_LOCATION'] == 'remote':
current_site = Site.objects.get_current()
if current_site.domain == 'www.socialscripter.com':
    # This is the real deal. It calls back to www.socialscripter.com!
    twitter_consumer_key = 'ENtBkTdrUlOcDCOhItdQ'                            
    twitter_consumer_secret = 'bBaZFBHI4J5TL5O85P70dwAqMNqj3anCe5fZM7iDVo'
    
    # This one calls back to vpssiam 122...:8040
    # tbak_test2
    # twitter_consumer_key = 'XvXX0bUHzHJVUeXOk2N5Q'                            
    # twitter_consumer_secret = 'waVaomZeHGV76WeZSKe1TjEpNIXohLhoLYRlBVN2x8'
else:
    # This one calls back to l.socialscripter.sfd
    # tbak_test
    twitter_consumer_key = 'mObmTCPGXXsDIe19VufXWg'
    twitter_consumer_secret = 'j0sJ4LCvW9maiNtfEAHMXV4iZHgjF2yaWYBMx3pNeHc'

facebook_api_key = 'cc085ef11d20234943fffa1f17a8d99e';
facebook_secret = 'e4608a1e1eecb413673aa457bd0e51ea';

class CreateUserForm(forms.Form):    
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField(required=True)
    captcha = CaptchaField()

class ScriptSocialTimingForm(forms.ModelForm):
    exacttime = kungfutimefield.KungfuTimeField()
    
    class Meta:
        model = ScriptSocialTiming
    
class ScriptForm(forms.ModelForm):
    # twitter_exacttime = kungfutimefield.KungfuTimeField()
    
    class Meta:
        model = Script
        # exclude = ['user', 'enabled', 'description', 'status', 'num_lines', 'scheduler_next_update_utc', 'localtz_utc_offset_secs', 'twitter_oauth_access_token', 'twitter_username', 'twitter_last_reply_id', 'twitter_num_posts', 'facebook_uid', 'facebook_num_posts', 'twitter_timing', 'facebook_timing' ]
        exclude = ['user', 'enabled', 'description', 'status', 'num_lines', 'scheduler_next_update_utc', 'localtz_utc_offset_secs', 'twitter_last_reply_id', 'twitter_num_posts', 'facebook_num_posts', 'twitter_timing', 'facebook_timing', ]
    

class ContactForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    message = forms.CharField( widget=forms.Textarea( attrs={'cols':80, 'rows':14}) )
    captcha = CaptchaField()


class BadUserException(Exception):
    """Used to output a generic message box in the HttpResponse if something goes wrong in code -- generally caused by the user doing something bad"""    
    def __init__(self,message):
        self.message = message   
    pass

class JsonException(Exception):
    """Used to output a JSON error message in the HttpResponse if something goes wrong in code"""    
    def __init__(self,message):
        self.message = message   
    pass

def generic_message(request, title, message):
    return render_to_response('main/generic_message.html', {"title":title, "message":message}, context_instance=RequestContext(request) )

# TB TODO - If DEBUG is False, then catch regular exceptions too and display generic_message + email to me.
class CatchUserException(object):
    """
    Decorator to catches the JsonException and output a json styled error message
    instead of regular html text. 
    """
    def __init__(self,view_func):
        self.view_func = view_func
        
    def __call__(self, request, *args, **kwargs):
        try:
            return self.view_func(request, *args, **kwargs)

        except HTTPError, e:
            return generic_message(request, "HTTP Error", 'The url "' + e.url + '" resulted in an unexpected HTTP error: ' + e.msg )
        
        except BadUserException, e:
            return generic_message(request, "Error", e.message)           
            
        except JsonException, e:
            return generic_message(request, "System Error", "Should not be seeing a Json error from CatchUserException decorator...")
         
# This returns the error information as Json data
# The above decorator returns the error information as a general window.
# We can only use 1, depending on how the returned data is expected. 
class CatchUserExceptionJson(object):         
    def __init__(self,view_func):
        self.view_func = view_func
        
    def __call__(self, request, *args, **kwargs):
        try:
            return self.view_func(request, *args, **kwargs)

        except HTTPError, e:
            out = {'result':'error', 'message':'The url "' + e.url + '" resulted in an unexpected HTTP error: ' + e.msg }
            return HttpResponse( simplejson.dumps(out) )
        
        except BadUserException, e:
            out = {'result':'error', 'message':e.message }
            return HttpResponse( simplejson.dumps(out) )           
            
        except JsonException, e:
            out = {'result':'error', 'message':e.message }
            return HttpResponse( simplejson.dumps(out) )
    
         
def get_object_or_baduser_error(klass, *args, **kwargs):
    try:        
        return klass.objects.get(*args, **kwargs)
    except klass.DoesNotExist:
        # Propagate a baduser error
        raise BadUserException('Could not find database object:' + str(klass) )   

def get_object_or_json_error(klass, *args, **kwargs):
    try:        
        return klass.objects.get(*args, **kwargs)
    except klass.DoesNotExist:
        # Propagate a json error
        raise JsonException('Could not find database object:' + str(klass) )   

def get_now_utc_tuple():
    """ Returns a tuple with a bunch of date/time components """
    now = datetime.datetime.now()
    secs = time.mktime(now.timetuple())
    
    # Make the datetime object with the first 6 tuple items: Y M D H M S.
    return time.gmtime(secs)

def get_now_utc_datetime():
    """ Return the current datetime for UTC """
    
    # Make the datetime object with the first 6 tuple items: Y M D H M S.
    return datetime.datetime( *get_now_utc_tuple()[0:6])

def get_now_utc_date():
    # Form the date using the first 3 tuble items: Y M D
    return datetime.date( *get_now_utc_tuple()[0:3]) 

def home(request):    
    if request.user.is_authenticated():
        return user_scripts(request)
    else:
        
        #return render_to_response('main/index.html', context_instance=RequestContext(request) )
        
        # Login form on the home page?
        form = AuthenticationForm(request)
        return render_to_response('main/index.html', {'form':form}, context_instance=RequestContext(request) )  

def get_clean_method_list(module, starting_param_index):
    raw_method_list = [ getattr(module,method) for method in dir(module) if callable(getattr(module, method)) and getattr(module, method) not in module.secret_methods ]
    method_list = []
    for method in raw_method_list:        
        # The first item in the argspec is all we care about
        # And filter away the 's' parameter since the program applies that for us (it's not a filter param relevant to users).
        # This is no longer true since replacers don't take a default string argument, so we need to chop at different places
        
        logging.debug(inspect.getargspec(method))
        
        argspec = inspect.getargspec(method)[0][starting_param_index:]
        #argspec = inspect.getargspec(method)
        #argspec = [method, 'bloo']
        
        method_list.append( {'name':method.__name__, 'param_list':argspec, 'doc':method.__doc__ } )
    
    return method_list

def help(request):
    
    # Get the help text for the available filters
    filter_list = get_clean_method_list(filters,1)
    
    replacer_list = get_clean_method_list(replacers,0)
    # Make replacers look like they do in scripts
    for replacer in replacer_list:
        replacer['name'] = '[_' + replacer['name'] + ']' 
    
    
        
    #return generic_message( request, "title", filter_list )
    
    return render_to_response('main/help.html', {'filter_list':filter_list, 'replacer_list':replacer_list}, context_instance=RequestContext(request) )

def contact(request):
    
    if request.method == 'POST': # If the form has been submitted...    
        
        form = ContactForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass                     
                      
            message = 'Message from: ' + form.cleaned_data['name']
            message = message + '\n'
            message = form.cleaned_data['message'] 

            logging.debug(message)

            send_mail('SocialScripter Message', message, form.cleaned_data['email'], ['tom@thomasbak.com'], fail_silently=True)

            return HttpResponseRedirect( reverse('socialscripter_django.main.views.contact_thankyou' ) )
         
    else:
        form = ContactForm()

    return render_to_response('main/contact.html', {"form":form}, context_instance=RequestContext(request) )    

def contact_thankyou(request):
    return generic_message(request, "Thank You", "Your message has been sent. We will get back to you as soon as possible.")

@login_required()
def create_script(request, script_name, script_data):
        
    new_script = Script()
    new_script.name = script_name
    new_script.user = request.user
    new_script.num_lines = len(script_data.splitlines())
    new_script.script_data = script_data
        
    #new_script.num_lines = len( fh.readlines() )
    facebook_timing = ScriptSocialTiming()
    facebook_timing.save()
    new_script.facebook_timing = facebook_timing
    
    twitter_timing = ScriptSocialTiming()
    twitter_timing.save()
    new_script.twitter_timing = twitter_timing    
    
    new_script.save()
    
    history = History()
    history.script = new_script
    history.user = request.user
    history.event = 0
    history.save()
        
    return HttpResponseRedirect( reverse('socialscripter_django.main.views.edit_script', args=[new_script.id] ) )

@login_required()       
def new_script(request):

    script_name = "New Script"
    script_data = open( getattr(settings, 'MEDIA_ROOT') + "scripts/default.txt", 'r').read()
    
    request.user.message_set.create(message='New script has been created')
    
    return create_script(request, script_name, script_data)    

@login_required()        
def copy_script(request, script_id):
    script = get_object_or_baduser_error(Script, pk=script_id)
    
    request.user.message_set.create(message='Script has been copied')
    
    return create_script(request, script.name + "_copy", script.script_data)
    
@CatchUserException
@login_required    
def delete_script(request, script_id):
    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user and not request.user.is_superuser:        
        raise BadUserException("You are not the owner of this script and can not delete it!")
    
    script.enabled = False
    script.save()
    
    request.user.message_set.create(message='Script has been deleted')
    
    history = History()
    history.script = script
    history.user = request.user
    history.event = 1
    history.save()
    
    return HttpResponseRedirect( reverse('socialscripter_django.main.views.user_scripts' ) )         

@CatchUserException
def edit_script(request, script_id, open_tab=None):
      
    # default script_id must be -1 instead of None since we will pass this to the template,
    # which doesn't know how to handle "None" as an integer...
    # See "var scriptID = {{script_id}};"       
      
    is_owner = True

    #script = get_object_or_404(Script, pk=script_id)
    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user:
        is_owner = False 
    
    if not is_owner and not script.sharing_enabled:        
        raise BadUserException("This script is not shared.")
    
    if script.enabled == False:        
        raise BadUserException("This script has been deleted.")
    
    def get_pretty_time(hour, min):                 
        ampm = 'AM'        
        if hour > 12:
            hour -= 12
            ampm = 'PM'
    
        return "%d:%02d %s"%(hour,min,ampm)                
            
    form = ScriptForm(instance=script)
    form_twitter_timing = ScriptSocialTimingForm(instance=script.twitter_timing, prefix="twitter_timing", initial={'exacttime':get_pretty_time(script.twitter_timing.exacttime.hour, script.twitter_timing.exacttime.minute)})
    
    form_facebook_timing = ScriptSocialTimingForm(instance=script.facebook_timing, prefix="facebook_timing", initial={'exacttime':get_pretty_time(script.facebook_timing.exacttime.hour, script.facebook_timing.exacttime.minute)})
        
    
    # Maybe you should explicitly click Run to get 'er started?
    # script_result = process_script_data(script.script_data, True)
    script_result = { 'debug':'Click "Run" to check your script' }
    
    if open_tab == 'Run':        
        script_result = process_script_data(script, script.script_data, False)       
        
    if open_tab == 'Debug':        
        script_result = process_script_data(script, script.script_data, True)
    
    # Default tab is the Edit tab
    if open_tab is None:
        open_tab = "Edit"
    
    # Get the twitter and facebook accounts associated with the user
    # Not needed. The form commands will create the select controls for us!
    # twitter_account_list = TwitterAccount.objects.all().filter(user__exact = request.user )
    # facebook_account_list = FacebookAccount.objects.all().filter(user__exact = request.user )
        
    # return render_to_response('main/test.html', {'script_text':script_text}, context_instance=RequestContext(request) )
    # return render_to_response('main/edit_script.html', {'script_name':script_name, 'script_text':script_text}, context_instance=RequestContext(request) )
    return render_to_response('main/edit_script.html', {'form':form, 'form_twitter_timing':form_twitter_timing, 'form_facebook_timing':form_facebook_timing, 'is_owner':is_owner, 'script_result':script_result, 'open_tab':open_tab}, context_instance=RequestContext(request) )
       
@login_required()
def user_history(request):
    history_list = History.objects.all().filter(user__exact = request.user )    
    return render_to_response('main/history.html', {"history_list":history_list}, context_instance=RequestContext(request) )

@login_required()
def user_scripts(request):
    
    script_list = Script.objects.all().filter(user__exact = request.user ).filter(enabled__exact = True)    
    return render_to_response('main/user.html', {"script_list":script_list}, context_instance=RequestContext(request) )

def browse(request):
    
    # TB TODO - Don't show your own scripts?
    script_list = Script.objects.all().filter(enabled__exact = True).filter(sharing_enabled__exact = True)    
    return render_to_response('main/browse.html', {"script_list":script_list}, context_instance=RequestContext(request) )

def process_script_data(script, script_data, debug_enabled):
    """Get the result of running a script in a nice dictionary"""
        
    lines = script_data.splitlines()
    
    lex = Lex(script=script)
    lex.debug_enabled = debug_enabled;
    lex.build_lex(lines)        
    
    sentences = []
    
    if len(lex.errors) == 0:
        for i in range(0,10):
            sentences.append(lex.generate_sentence())
            
    clean_warnings = [ (warning[0], html.escape(warning[1])) for warning in lex.warnings ]
    clean_errors = [ (error[0], html.escape(error[1])) for error in lex.errors ]
        
    out = {"warnings":clean_warnings, "errors":clean_errors, "sentences":sentences}
    if debug_enabled:
        re_spaces = re.compile(r'(\s*)')
        d = ""
        for line in lex.debug_buffer.splitlines():                       
            
            # Convert spaces to a SPAN
            match = re_spaces.search(line)            
            d = d + '<div class="debugLine" style="width:%dpx;">&nbsp;</div><div class="debugText">%s</div><div class="clear"></div>' % (len(match.group(1))*5, html.escape(line) ) 
            
        out["debug"] = d
        
    return out


@CatchUserException
@login_required
def oauth_twitter_start(request, script_id):
    
    # Check that user owns script!!!
    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user:        
        raise BadUserException("You are not the owner of this script and can not modify the twitter settings!")
    
    oatwitter = oauthtwitter.OAuthApi(twitter_consumer_key, twitter_consumer_secret)
    request_token = oatwitter.getRequestToken()

    authorization_url = oatwitter.getAuthorizationURL(request_token)
    
    # Store the script that we are working on, so that we know what script to return
    # to when we get back from twitter.com
    # We don't need to do this with facebook since we can tell facebook to point back to a particular page.
    # So we just point to the particular script's page...
    request.session["oauth_twitter_return_script_id"] = script_id 
    request.session["oauth_twitter_request_token"] = request_token

    # Go to twitter!
    # We will return to oauth_twitter_success when done...
    return HttpResponseRedirect( authorization_url )
       
    
@CatchUserException
@login_required
def oauth_twitter_success(request):
    
    if ("oauth_twitter_request_token" not in request.session) or ("oauth_twitter_request_token" not in request.session):
        raise BadUserException("Something went wrong authenticating with Twitter. Make sure cookies are enabled on your browser and try again.")  
    
    script_id = request.session["oauth_twitter_return_script_id"]
    
    # Check that user owns script!!!    
    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user:        
        raise BadUserException("You are not the owner of this script and can not modify the twitter settings!")
    
    oatwitter = oauthtwitter.OAuthApi(twitter_consumer_key, twitter_consumer_secret, request.session["oauth_twitter_request_token"])
    access_token = oatwitter.getAccessToken()
      
    # Replaced by TwitterAccount  
    # script.twitter_oauth_access_token = access_token.to_string()     
        
    twitter = oauthtwitter.OAuthApi(twitter_consumer_key, twitter_consumer_secret, oauthtwitter.oauth.OAuthToken.from_string(access_token.to_string()) )
    
    userinfo = twitter.GetUserInfo()
    # Replaced by TwitterAccount
    # script.twitter_username = userinfo.GetScreenName()
    
    # See if this screen name has already been added to the list of available twitter accounts!
    # If so, then don't add it again. Just select the account for this script and be done.
    # Otherwise, create a new account.
    try:
        twitter_account = TwitterAccount.objects.get(user__exact = request.user, screen_name__exact = userinfo.GetScreenName() )
    except TwitterAccount.DoesNotExist:
        
        twitter_account = TwitterAccount()
        twitter_account.user = request.user
        twitter_account.oauth_access_token = access_token.to_string()
        twitter_account.screen_name = userinfo.GetScreenName()
        twitter_account.save() 
    
    # We now have either a newly created twitter account, or the old twitter account that the dude is trying to create again.
    # So associate that with our script.    
    script.twitter_account = twitter_account
        
    script.scheduler_next_update_utc = "2000-01-01"
    script.twitter_timing.frequency = 0     
    
    
    # Update the follower list with all initial followers.
    # Otherwise old followers will be processed as a "new" follower on the first update.
    all_followers = twitter.GetFollowers()
    all_followers = [ a.GetScreenName() for a in all_followers ]
    
    current_follower_list = TwitterFollower.objects.all()
    current_follower_list = [ a.follower_username for a in current_follower_list ]
    
    new_followers = [ a for a in all_followers if a not in current_follower_list ]        
    
    # Any followers not in the followers list for this script should then follow this follower!
    # And potentially also send a direct message!        
    for new_follower in new_followers:
        tf = TwitterFollower()
        tf.follower_username = new_follower
        tf.script = script
        tf.save()
    
    
    # Get the most recent reply, so that we don't process that
    all_replies = twitter.GetReplies()
    last_id = 0
    for reply in all_replies:
        if reply.id > last_id:
            last_id = reply.id     
    
    script.twitter_last_reply_id = last_id
    
    
    
    script.save()
    
    #twitter.PostUpdate("Test django 2")
    
    # TB TODO - save the access token or whatever
       
    request.user.message_set.create(message='Your Twitter account has been authenticated!')
    
    #return HttpResponse( "Posted!" );
    #user = twitter.GetUserInfo()
    
    #return HttpResponse( request.session["twitter_token_to_script-" + request_token.to_string() ] );
    #return HttpResponse( request.session["oauth_twitter_return_script_id"] );
    logging.debug( userinfo.GetTimeZone() )
    logging.debug( userinfo.GetUtcOffset() )
    #return HttpResponse( "foo!" );
    
    #return generic_message(request, "yo") 
    
    # Return back to the edit page, with the twitter tab open!
    return HttpResponseRedirect( reverse('socialscripter_django.main.views.edit_script', args=[script_id, 'Twitter'] ) )      
    
@CatchUserException
@login_required
def oauth_twitter_remove(request, script_id):
    
    # TB TODO
    raise BadUserException("To remove twitter access for this script, change the Twitter account value in the select box...")
    
    """
    # Check that user owns script!!!
    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user:        
        raise BadUserException("You are not the owner of this script and can not modify the twitter settings!")
    
    script.twitter_oauth_access_token = ''
    script.twitter_username = ''
    script.save()
    
    request.user.message_set.create(message='Your Twitter account is no longer authenticated!')
    
    return HttpResponseRedirect( reverse('socialscripter_django.main.views.edit_script', args=[script_id, 'Twitter'] ) )
    """


@CatchUserException
@login_required
def auth_facebook_start(request, script_id):

    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user:        
        raise BadUserException("You are not the owner of this script and can not modify the twitter settings!")   
    

    # It appears I can just go here and it'll take care of everything for me?
    # No need to use the facebook library at all to kick this thing off
    success_abs_url = 'http://%s%s' % (Site.objects.get_current().domain, reverse('socialscripter_django.main.views.auth_facebook_success', args=[script.id] ) )
    failure_abs_url = 'http://%s%s' % (Site.objects.get_current().domain, reverse('socialscripter_django.main.views.edit_script', args=[script.id,'Facebook'] ) )
    fbconnect_url = 'http://www.facebook.com/connect/prompt_permissions.php?api_key=%s&v=1&ext_perm=publish_stream,offline_access&display=popup&next=%s&locale=en_US&cancel=%s&canvas=0&return_session=1&source=login&fbconnect=true&offline_access=0&required=1'%(facebook_api_key,success_abs_url,failure_abs_url)
    return HttpResponseRedirect( fbconnect_url )
    

@CatchUserException
@login_required
def auth_facebook_success(request, script_id):

    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user:        
        raise BadUserException("You are not the owner of this script and can not modify the twitter settings!")
    
    
    try:    
        result = request.GET['session']        
        result = simplejson.loads(result)

        logging.debug(result)
        
        fb = facebook.Facebook(facebook_api_key, facebook_secret)

        # This works
        fb.uid = result['uid']
        fb.auth_token = result['session_key']        
        fb.session_key = result['session_key']
        
        myself = fb.users.getInfo(fb.uid, ['name'])
        logging.debug(myself)
        
        # Just like with twitter accounts, try to create a new FacebookAccount. If it already exists, just select it
        try:
            facebook_account = FacebookAccount.objects.get(user__exact = request.user, uid__exact = result['uid'] )
            
        except FacebookAccount.DoesNotExist:
        
            facebook_account = FacebookAccount()
            facebook_account.user = request.user
            facebook_account.uid = result['uid']
            facebook_account.session_key = result['session_key']
            facebook_account.secret = result['secret']
            facebook_account.screen_name = myself[0]['name']
            facebook_account.save()
        
        script.facebook_account = facebook_account
        script.facebook_timing.frequency = 0
        script.save()

        
        logging.debug(fb.ext_perms)
        friends = fb.friends.get()
        logging.debug(friends)
        
    
    except KeyError, e:
        # If you previously authenticated your facebook account, and then removed the authentication (which just locally changes the database),
        # then if you try to authenticate your script again, facebook.com will immediately return to this success page with no GET variables set.
        # So the best we really can do is hope that the account is already created...
        # Is there some way to force a login or force the session stuff to display? Otherwise the user will be confused if trying
        # to connect this to a facebook account other than the one he's currently using...
        return HttpResponseRedirect( reverse('socialscripter_django.main.views.edit_script', args=[script_id, 'Facebook'] ) )            
    
    except facebook.FacebookError, e:
        
        # Error 102 means the session has expired.
        # Delete the cookie and send the user to Facebook to login
        logging.debug(e)
        if e.code == 102:
            return generic_message(request, "Error", "Expired session!" )
        
        return generic_message(request, "Error", "Generic Facebook error" + str(e.code) )
        
                
    
        
            
    
    """
    friends = fb.friends.get()
    logging.debug(friends)
    friends = fb.users.getInfo(friends[0:5], ['name', 'birthday', 'relationship_status'])
    logging.debug(friends)
    myself = fb.users.getInfo(fb.uid, ['name', 'birthday', 'relationship_status'])
    logging.debug(myself)
    """
    
    #return generic_message(request, "yo", "Success?" )
    return HttpResponseRedirect( reverse('socialscripter_django.main.views.edit_script', args=[script_id, 'Facebook'] ) )    

@CatchUserException
@login_required
def auth_facebook_remove(request, script_id):
    
    # Check that user owns script!!!
    script = get_object_or_baduser_error(Script, pk=script_id)
    if script.user != request.user:        
        raise BadUserException("You are not the owner of this script and can not modify the twitter settings!")
    
    script.facebook_name = ''
    script.facebook_uid = 0
    script.facebook_secret = ''
    script.facebook_session_key = ''
    script.save()
    
    request.user.message_set.create(message='Your Facebook account is no longer authenticated!')
    
    return HttpResponseRedirect( reverse('socialscripter_django.main.views.edit_script', args=[script_id, 'Facebook'] ) )



@CatchUserExceptionJson
def email_script_json(request):
    if request.method != 'POST':        
        raise JsonException("You must POST the data!")

    if 'email_address' not in request.POST or 'script_id' not in request.POST:        
        raise JsonException("You must POST email_address and script_id")
    
    script_id = int( request.POST.get('script_id', -1) )
    if script_id is -1:    
        raise JsonException("Invalid script ID, or script_id was not set")
    
    script = get_object_or_json_error(Script, pk=script_id)
    
    message = 'Hello,\n\nSocialScripter user ' + request.user.username + ' has sent you a link to a script that you might be interested in.\n\n'
    # message += 'Sample randomized output:\n'
    message += 'Script name: ' + script.name + '\n\n'
    message += 'You can view and run the script by clicking on this link: http://www.socialscripter.com' + reverse('socialscripter_django.main.views.edit_script', args=[script.id] ) + '\n\n'
    message += 'Regards,\nThe SocialScripter Team'
    
    #logging.debug(message)
    
    # TB TODO - Actually email the thing!
    send_mail('SocialScripter Message', message, request.POST['email_address'], [request.user.email], fail_silently=True)
    
    output = simplejson.dumps( {'result':'success'} )
    return HttpResponse( output )    

@CatchUserExceptionJson
def post_twitter_json(request):
    if request.method != 'POST':        
        raise JsonException("You must POST the data!")
    
    if 'message' not in request.POST:        
        raise JsonException("Message is not in the POST")
    
    script_id = int( request.POST.get('script_id', None) )
    if script_id is None:    
        raise JsonException("Invalid script ID, or script_id was not set.")
    
    script = get_object_or_json_error(Script, pk=script_id)
    if request.user.id != script.user.id:        
        raise JsonException("You are not the owner of this script.")
    
    twitter = oauthtwitter.OAuthApi(twitter_consumer_key, twitter_consumer_secret, oauthtwitter.oauth.OAuthToken.from_string(script.twitter_account.oauth_access_token) )    
    twitter.PostUpdate(request.POST['message'])
    
    out = { 'result':'success' }
    return HttpResponse( simplejson.dumps(out) )        

    
@CatchUserExceptionJson
def run_script_json(request, script_id):
    
    if request.method != 'POST':        
        raise JsonException("You must POST the script data!")
    
    if 'script_data' not in request.POST:        
        raise JsonException("You must POST the script data to \"script_data\" variable!")

    debug_enabled = False;
    if 'show_debug' in request.POST:
        debug_enabled = request.POST['show_debug'];
        
    script = get_object_or_baduser_error(Script, pk=script_id)
    is_owner = True
    if script.user != request.user:
        is_owner = False 
    
    if not is_owner and not script.sharing_enabled:        
        raise BadUserException("This script is not shared.")
    
    if script.enabled == False:        
        raise BadUserException("This script has been deleted.")
        
    out = process_script_data(script, request.POST['script_data'], debug_enabled)
            
    output = simplejson.dumps( out )
    return HttpResponse( output )


def register(request):    
    
    if request.method == 'POST': # If the form has been submitted...
        
        # This resulted in an error when somebody tried to create an account?
        # It's possible it was some confused automated system. Just get rid of it anyways.
        # next = request.POST['next']
        
        form = CreateUserForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass                     
                      
            
            # Does the user already exist?
            is_taken = True            
            try:
                User.objects.get(username__exact=form.cleaned_data['username'])
            except User.DoesNotExist:
                is_taken = False
            
            if is_taken:
                form.errors['username'] = "User name is already taken"     
            
            else:                
            
                # Create the user!
                user = User.objects.create_user( form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'] )
                user.save()
    
                # Automatically log in the user
                # Must call authenticate first
                user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
                login(request,user)
    
                #
                # Send email to the address with user and password info!
                #
                message = 'Thank you for signing up with SocialScripter.\n\n'
                message += 'Your username: ' + form.cleaned_data['username'] + '\n\n'                
                message += 'If you have any questions or comments, please let us know. You can reach us by visiting http://www.socialscripter.com/contact\n\n'
                message += 'Regards,\nThe SocialScripter Team\nhttp://www.socialscripter.com'
                send_mail('Welcome to SocialScripter!', message, 'noreply@socialscripter.com', [form.cleaned_data['email']], fail_silently=True)
                
                request.user.message_set.create(message='Your new account has been created!')
    
                return HttpResponseRedirect( reverse('socialscripter_django.main.views.home' ) )
         
    else:
        form = CreateUserForm( initial = {'name':'foo!'} )

    return render_to_response('main/register.html', {"form":form}, context_instance=RequestContext(request) )


@CatchUserExceptionJson
def save_script_json(request):
    """Create and edit new scripts. 
    Returns the result as a JSON string.
    Works in conjuction with edit_script() view"""
    
    if request.method != 'POST':
        raise JsonException("You must pass values with POST")
    
    if 'script_id' not in request.POST:
        raise JsonException("You must POST the script ID to \"script_id\" variable!")
       
    script_id = int( request.POST.get('script_id', -1) )
    
    script = get_object_or_json_error(Script, pk=script_id)
    
    # Verify that the logged-in user is the owner of the script        
    if request.user.id != script.user.id:            
        raise JsonException("User is not the owner of this script")               

    # Create a copy of the script...
    # Then we can save the new script, and see if any settings have changed.
    # Maybe use deepcopy? 
    old_timings = []
    old_timings.append( (script.twitter_timing, copy.copy(script.twitter_timing)) )
    old_timings.append( (script.facebook_timing, copy.copy(script.facebook_timing)) )
    
    
    # TB TODO - Should return a JSON error
    #script = get_object_or_404( Script, user__exact = request.user )
        
    
    # Save the created or edited tour using posted data
    form = ScriptForm(request.POST, request.FILES, instance=script)
    form_twitter_timing = ScriptSocialTimingForm(request.POST, request.FILES, instance=script.twitter_timing, prefix="twitter_timing")
    form_facebook_timing = ScriptSocialTimingForm(request.POST, request.FILES, instance=script.facebook_timing, prefix="facebook_timing")
    if form.is_valid() and form_twitter_timing.is_valid() and form_facebook_timing.is_valid():
        
        # Try building the script. If build error, report that in the JSON. Which will then say "script has errors. Twitter features will not work"
        # But even if there are errors, save the thing
        
        #lines = script.script_data.splitlines()
        lines = form.cleaned_data['script_data'].splitlines()
        lex = Lex()
        lex.build_lex(lines)            
        
        script = form.save(commit=False)
        script.user = request.user
        script.num_lines = len(lines)
        if len(lex.errors) > 0:
            script.status = 1
        else:
            script.status = 0
            
        form_twitter_timing.save()
        form_facebook_timing.save()
    
        # TB TODO - Check if twitter/facebook settings have changed
        # If they have, schedule an instant scheduler run.
        # The instant scheduler will cancel all previously scheduled posts.
        # The scheduler will then try to create posts for this day
        # Any created posts that should be posted BEFORE the current time are ignored.
        # This way we don't need to wait until midnight localtz before creating the first posts...
        for old_timing in old_timings:
            if( (old_timing[0].frequency != old_timing[1].frequency) or 
                (old_timing[0].wakingtime_range_min != old_timing[1].wakingtime_range_min) or
                (old_timing[0].wakingtime_range_max != old_timing[1].wakingtime_range_max) or
                (old_timing[0].anytime_range_min != old_timing[1].anytime_range_min) or
                (old_timing[0].anytime_range_max != old_timing[1].anytime_range_max) or
                (old_timing[0].exacttime != old_timing[1].exacttime) ):
                
                # Force an update next time process_script cron job runs
                script.scheduler_next_update_utc = "2000-01-01"            
        
             
        # Recalculate the timezone
        # See models.py for explanation of what these numbers mean                        
        val = "%06d"%form.cleaned_data['localtz']            
        utc_offset = ( int(val[1])*10 + int(val[2]) ) * 60
        utc_offset += ( int(val[3])*10 + int(val[4]) )
        utc_offset *= 60 # Convert to seconds
        if val[0] is '1':
            utc_offset *= -1
        script.localtz_utc_offset_secs = utc_offset
        
        
        script.save()
         
        # Is this neccessary?            
        if len(lex.errors) > 0:               
            raise JsonException("The script was saved, but your script data has errors.")
                    
        return HttpResponse( simplejson.dumps({ 'result':'success', 'script_id':script_id }) )            
    
    else:            
        raise JsonException("Form is not valid:" + str(form.errors) + str(form_twitter_timing.errors) + str(form_facebook_timing.errors) )
    
        
    
    raise JsonException("Unknown error saving script")   
