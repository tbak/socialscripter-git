import datetime
import inspect
import logging
import os
import re
import sys
import tempfile
import time
import traceback
import urlparse

try:
    import pygments
    import pygments.lexers
    import pygments.formatters
    import pygments.styles
except ImportError:
    pygments = None

import django
from django.conf import settings
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.db import connection
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import loader
from django.utils.cache import add_never_cache_headers
from django.utils.datastructures import SortedDict
from django.utils.html import escape
try:
    from django.utils.encoding import smart_str
except ImportError:
    # Older versions of Django don't have smart_str, but because they don't
    # require Unicode, we can simply fake it with an identify function.
    smart_str = lambda s: s
try:
    from django.utils.safestring import mark_safe
except ImportError:
    # Older versions of Django don't have mark_safe, so we have to escape
    # manually when required.
    mark_safe = None
from django.utils.functional import curry

from djangologging import SUPPRESS_OUTPUT_ATTR, getLevelNames
from djangologging.handlers import ThreadBufferedHandler


""" Regex to find where to place the logging head in a (X)HTML document. """
head_pattern = getattr(settings, 'LOGGING_HEAD_PREPEND_PATTERN', '(</head>)')
place_head_re = re.compile(head_pattern, re.M | re.I)

""" Regex to find where to place the logging body in a (X)HTML document. """
body_pattern = getattr(settings, 'LOGGING_BODY_PREPEND_PATTERN', '(</body>)')
place_body_re = re.compile(body_pattern, re.M | re.I)

logging_level = getattr(settings, 'LOGGING_LEVEL', logging.NOTSET)

# Initialise and register the handler
handler = ThreadBufferedHandler()
logging.root.setLevel(logging_level)
logging.root.addHandler(handler)

# Because this logging module isn't registered within INSTALLED_APPS, we have
# to use (or work out) an absolute file path to the templates and add it to 
# TEMPLATE_DIRS.
try:
    template_path = settings.LOGGING_TEMPLATE_DIR
except AttributeError:
    template_path = os.path.join(os.path.dirname(__file__), 'templates')
settings.TEMPLATE_DIRS = (template_path,) + tuple(settings.TEMPLATE_DIRS)

try:
    intercept_redirects = settings.LOGGING_INTERCEPT_REDIRECTS
except AttributeError:
    intercept_redirects = False

try:
    logging_output_enabled = settings.LOGGING_OUTPUT_ENABLED
except AttributeError:
    logging_output_enabled = settings.DEBUG

try:
    logging_show_metrics = settings.LOGGING_SHOW_METRICS
except AttributeError:
    logging_show_metrics = True

profiling_enabled = False
try:
    from djangologging.profiling import profilers
    profiler_name = settings.LOGGING_PROFILER
    profiler = profilers[profiler_name]
except AttributeError:
    pass
except KeyError:
    raise "LOGGING_PROFILER must be one of %s." % profilers.keys()
except:
    pass
else:
    profiling_enabled = True 

try:
    logging_show_hints = settings.LOGGING_SHOW_HINTS
except AttributeError:
    logging_show_hints = True

try:
    logging_log_sql = settings.LOGGING_LOG_SQL
except AttributeError:
    logging_log_sql = False

try:
    logging_rewrite_content_types = settings.LOGGING_REWRITE_CONTENT_TYPES
except AttributeError:
    logging_rewrite_content_types = ('text/html',)

_python_libs_path = os.path.normcase(os.path.realpath(os.path.dirname(os.__file__) + os.sep))
_django_path = os.path.normcase(os.path.realpath(os.path.dirname(django.__file__) + os.sep))
_admin_path = os.path.normcase(os.path.realpath(os.path.dirname(admin.__file__) + os.sep))
_logging_path = os.path.normcase(os.path.realpath(os.path.dirname(logging.__file__) + os.sep))
_djangologging_path = os.path.normcase(os.path.realpath(os.path.dirname(__file__) + os.sep))

def get_meaningful_frame(number_of_context_lines=11):
    """
    Try to find the meaningful frame, rather than just using one from
    the innards of the Django or logging code.
    """
    framesInfo = inspect.getouterframes(inspect.currentframe(), number_of_context_lines)
    for (frame, filename, lineno, function, code_context, index) in framesInfo:
        filename = os.path.normcase(os.path.realpath(frame.f_globals["__file__"]))
        is_framework = (\
            filename.startswith(_python_libs_path) or \
            filename.startswith(_django_path) or \
            filename.startswith(_logging_path) or \
            filename.startswith(_djangologging_path) or \
            filename.startswith(_admin_path) \
        )
        if not is_framework:
            return frame, code_context, lineno, index

    for (frame, filename, lineno, function, code_context, index) in framesInfo:
        if 'process_' in function:
            return frame, code_context, lineno, index


if logging_log_sql:
    # Define a new logging level called SQL
    logging.SQL = logging.DEBUG + 1
    logging.addLevelName(logging.SQL, 'SQL')
    
    # Define a custom function for creating log records
    def make_sql_record(frame, original_makeRecord, sqltime, self, *args, **kwargs):
        args = list(args)
        len_args = len(args)
        if frame:
            if len_args > 2:
                args[2] = frame.f_globals['__file__']
            else:
                kwargs['fn'] = frame.f_globals['__file__']
            if len_args > 3:
                args[3] = frame.f_lineno
            else:
                kwargs['lno'] = frame.f_lineno
            if len_args > 7:
                args[7] = frame.f_code.co_name
            elif 'func' in kwargs:
                kwargs['func'] = frame.f_code.co_name
        rv = original_makeRecord(self, *args, **kwargs)
        rv.__dict__['sqltime'] = '%d' % sqltime
        return rv
    
    class SqlLoggingList(list):
        def append(self, object):
            frame_info = get_meaningful_frame()
            sqltime = float(object['time']) * 1000
            # Temporarily use make_sql_record for creating log records
            original_makeRecord = logging.Logger.makeRecord
            logging.Logger.makeRecord = curry(make_sql_record, frame_info[0], original_makeRecord, sqltime)
            logging.getLogger().log(logging.SQL, object['sql'])
            logging.Logger.makeRecord = original_makeRecord
            list.append(self, object)


_makeRecord = logging.Logger.makeRecord
def enhanced_make_record(self, *args, **kwargs):
    """Enahnced makeRecord that captures the source code and local variables of
    the code logging a message."""
    rv = _makeRecord(self, *args, **kwargs)
    number_of_context_lines = 11
    frame, source_lines, lineno, idx = get_meaningful_frame(number_of_context_lines)
    if source_lines:
        start = lineno - idx
        highlights = [idx + 1]
        rv.__dict__['source_lines'] = python_to_html(''.join(source_lines), start, highlights)
    if frame:
        rv.__dict__['local_variables'] = frame.f_locals.items()
        try:
            request = filter(lambda var: var[0] == 'request', rv.__dict__['local_variables'])[0][1]
            session = request.session._session_cache
            rv.__dict__['local_variables'].append(('session', session))
        except:
            pass
        tracebackstrings = ['Traceback (most recent call last):\n',] + traceback.format_stack(frame)
        rv.__dict__['traceback'] = traceback_to_html(''.join(tracebackstrings))
    return rv

logging.Logger.makeRecord = enhanced_make_record


_redirect_statuses = {
    301: 'Moved Permanently',
    302: 'Found',
    303: 'See Other',
    307: 'Temporary Redirect'}


if profiling_enabled:
    # Because this logging module isn't registered within INSTALLED_APPS, we have
    # to manually inject the views into the root urlconf.
    urlconf_module = __import__(settings.ROOT_URLCONF).urls
    urlconf_module.urlpatterns += urlconf_module.patterns('',
        (r'^djangologging/', urlconf_module.include('djangologging.urls')),
    )


def format_time(record):
    time = datetime.datetime.fromtimestamp(record.created)
    return '%s,%03d' % (time.strftime('%H:%M:%S'), record.msecs)

def generate_request_id():
    try:
        import uuid
    except ImportError:
        return str(int(time.time()))
    else:
        return str(uuid.uuid1())

def sql_to_html(sql):
    if pygments:
        try:
            lexer = {
                'mysql': pygments.lexers.MySqlLexer,
                'sqlite3': pygments.lexers.SqlLexer,
                }[settings.DATABASE_ENGINE]
        except KeyError:
            lexer = pygments.lexers.SqlLexer
        html = pygments.highlight(sql, lexer(),
            pygments.formatters.HtmlFormatter(cssclass='sql_highlight'))
        
        # Add some line breaks in appropriate places
        html = html.replace('<span class="k">', '<br /><span class="k">')
        html = re.sub(r'(<pre>\s*)<br />', r'\1', html)
        html = re.sub(r'(<span class="k">[^<>]+</span>\s*)<br />(<span class="k">)', r'\1\2', html)
        html = re.sub(r'<br />(<span class="k">(IN|LIKE)</span>)', r'\1', html)
        
        # Add a space after commas to help with wrapping
        html = re.sub(r'<span class="p">,</span>', '<span class="p">, </span>', html)
    
    else:
        html = '<div class="sql_highlight"><pre>%s</pre></div>' % escape(sql)
    
    if mark_safe:
        html = mark_safe(html)
    return html

def python_to_html(python, linenostart=1, hl_lines=()):
    if pygments:
        html = pygments.highlight(python,
            pygments.lexers.PythonLexer(),
            pygments.formatters.HtmlFormatter(
                linenos='inline', linenostart=linenostart, hl_lines=hl_lines
                ))
    
    else:
        lines = python.split('\n')
        mx = len(str(linenostart + len(lines)))
        python = '\n'.join(['%*d %s' % (mx, i+1, l) for i, l in enumerate(lines)])
        html = '<pre>%s</pre>' % escape(python)
    
    if mark_safe:
        html = mark_safe(html)
    return html
    

def traceback_to_html(traceback):
    if pygments:
        html = pygments.highlight(
            traceback,
            pygments.lexers.PythonTracebackLexer(),
            pygments.formatters.HtmlFormatter()
        )

    else:
        html = '<pre>%s</pre>' % escape(traceback)

    if mark_safe:
        html = mark_safe(html)
    return html


class LoggingMiddleware(object):
    """
    Middleware that appends the messages logged during the request to the
    response (if the response is HTML).
    """

    def process_request(self, request):
        handler.clear_records()
        if logging_log_sql:
            connection.queries = SqlLoggingList(connection.queries)
        request.logging_start_time = time.time()

    def process_view(self, request, callback, callback_args, callback_kwargs):
        if profiling_enabled:
            self.tempfileno, self.tempfile = tempfile.mkstemp()
            return profiler.profile(self.tempfile, callback, request, *callback_args, **callback_kwargs)


    def process_response(self, request, response):
        is_ajax = request.is_ajax() 
        is_allowed_ADDR = request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS
        is_supressed = getattr(response, SUPPRESS_OUTPUT_ATTR, False)

        if not logging_output_enabled or not is_allowed_ADDR or is_ajax or is_supressed:
            return response

        if profiling_enabled:
            os.close(self.tempfileno)
            recent_profiles = request.session.get('djangologging.recent_profiles', {})
            self.request_id = generate_request_id()
            recent_profiles[self.request_id] = self.tempfile
            request.session['djangologging.recent_profiles'] = recent_profiles

        if intercept_redirects and \
                response.status_code in _redirect_statuses and \
                len(handler.get_records()):
            response = self._handle_redirect(request, response)

        for content_type in logging_rewrite_content_types:
            if response['Content-Type'].startswith(content_type):
                self._rewrite_html(request, response)
                add_never_cache_headers(response)
                break

        return response


    def _get_and_clear_records(self):
            records = handler.get_records()
            handler.clear_records()
            for record in records:
                record.formatted_timestamp = format_time(record)
                message = record.getMessage()
                if record.levelname == 'SQL':
                    record.formatted_message = sql_to_html(message)
                else:
                    record.formatted_message = escape(message)
            return records

    def _rewrite_html(self, request, response):
        if not hasattr(request, 'logging_start_time'):
            return
        hints = {
            'pygments': logging_log_sql and not pygments,
            'profiling': not profiling_enabled and False,
            }
        context = {
            'records': self._get_and_clear_records(),
            'levels': getLevelNames(),
            'elapsed_time': (time.time() - request.logging_start_time) * 1000, # milliseconds
            'query_count': -1,
            'logging_log_sql': logging_log_sql,
            'logging_show_metrics': logging_show_metrics,
            'logging_show_hints': logging_show_hints,
            'hints': dict(filter(lambda (k, v): v, hints.items())),
            }
        if profiling_enabled:
            context['valid_profilers'] = profilers.keys()
        
        if profiling_enabled:
            context['profiler_name'] = profiler_name
            context['profile_url'] = reverse('djangologging.views.profile', 
                kwargs={'request_id': self.request_id})

        if settings.DEBUG and logging_show_metrics:
            context['query_count'] = len(connection.queries)
            if context['query_count'] and context['elapsed_time']:
                context['query_time'] = sum(map(lambda q: float(q['time']) * 1000, connection.queries))
                context['query_percentage'] = context['query_time'] / context['elapsed_time'] * 100

        header = smart_str(loader.render_to_string('logging.css'))
        footer = smart_str(loader.render_to_string('logging.html', context))

        if place_head_re.search(response.content) and place_body_re.search(response.content):
            def safe_prepend(prependant):
                def _prepend(match):
                    return '%s%s' % (prependant, match.group(0)) 
                return _prepend
            response.content = place_head_re.sub(safe_prepend(header), response.content)
            response.content = place_body_re.sub(safe_prepend(footer), response.content)
        else:
            # Despite a Content-Type of text/html, the content doesn't seem to
            # be sensible HTML, so just append the log to the end of the
            # response and hope for the best!
            response.write(footer)

    def _handle_redirect(self, request, response):
        if hasattr(request, 'build_absolute_url'):
            location = request.build_absolute_uri(response['Location'])
        else:
            # Construct the URL manually in older versions of Django
            request_protocol = request.is_secure() and 'https' or 'http'
            request_url = '%s://%s%s' % (request_protocol,
                request.META.get('HTTP_HOST'), request.path)
            location = urlparse.urljoin(request_url, response['Location'])
        data = {
            'location': location,
            'status_code': response.status_code,
            'status_name': _redirect_statuses[response.status_code]}
        response.content = loader.render_to_string('redirect.html', data)
        response.status_code = 200
        add_never_cache_headers(response)
        return response
