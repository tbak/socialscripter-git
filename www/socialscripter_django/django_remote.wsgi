import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'socialscripter_django.settings'
os.environ['DJANGOTEST_SERVER_LOCATION'] = 'remote'

sys.path.append('/home/tbak/p/socialscripter/www')
sys.path.append('/home/tbak/p/socialscripter/www/socialscripter_django')

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
