#!/usr/bin/env python
import django
from django.core.management import setup_environ
import os
import re

# Options parsing needs to be at the top of this file so that os.environ can be set when settings is imported.
if __name__ == "__main__":
    
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-r", "--remote", action="store_const", const="remote", dest="server_location", help="Use the remote applicaton twitter settings")
    parser.add_option("-l", "--local", action="store_const", const="local", default="local", dest="server_location", help="Use the local callback twitter settings")    

    (options, args) = parser.parse_args()
    os.environ['DJANGOTEST_SERVER_LOCATION'] = options.server_location

try:
    import settings # Assumed to be in the same directory.
except ImportError:
    import sys
    sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.exit(1)

setup_environ(settings)
from main.models import Script, ScriptSocialTiming
from django.contrib.auth.models import User

if __name__ == "__main__":
        
    user = User.objects.get(username__exact='tutorial')    
    
    for root, dirs, files in os.walk( getattr(settings, 'MEDIA_ROOT') + "scripts/tutorial/" ):
        for name in files:
            
            # File must end with .txt
            if re.compile('.txt').search(name) is None:
                continue                        
                
            display_name = name[:-4]
            print display_name
            
            datafile_path = os.path.join(root, name)
            fh = open( datafile_path, "r" )
            
            #for line in fh.readlines():
            #    print line            
            
            new_script = Script()
            new_script.name = display_name
            new_script.user = user
            new_script.script_data = fh.read()
            
            fh.seek(0)
            new_script.num_lines = len( fh.readlines() )

	    facebook_timing = ScriptSocialTiming()
	    facebook_timing.save()
	    new_script.facebook_timing = facebook_timing

	    twitter_timing = ScriptSocialTiming()
	    twitter_timing.save()
	    new_script.twitter_timing = twitter_timing
            
            new_script.save()
            
            
    
    print "Done inserting file scripts! Now bugger off!"
