# Django settings for socialscripter project.

# TB -  Used to detect whether we are on the local test machine or the remote production machine (environ variable set in the wsgi files)
import os


ADMINS = (
    ('Tom Bak', 'tom@thomasbak.com'),
)

MANAGERS = ADMINS


DATABASE_ENGINE = 'mysql'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = 'socialscripter'             # Or path to database file if using sqlite3.
DATABASE_USER = 'slartibartfast'             # Not used with sqlite3.
DATABASE_PASSWORD = 'dbx34qp7'         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'


# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

if (not 'DJANGOTEST_SERVER_LOCATION' in os.environ) or (os.environ['DJANGOTEST_SERVER_LOCATION'] != 'remote'):

    #
    # LOCAL / DEVELOPMENT
    #
    
    SITE_ID = 1
    
    # Absolute path to the directory that holds media.
    # Example: "/home/media/media.lawrence.com/"
    MEDIA_ROOT = 'C:/p2/socialscripter/www/media/'
    
    # URL that handles the media served from MEDIA_ROOT. Make sure to use a
    # trailing slash if there is a path component (optional in other cases).
    # Examples: "http://media.lawrence.com", "http://example.com/media/"
    MEDIA_URL = 'http://l.media.socialscripter.com/'
    
    TEMPLATE_DIRS = (
        # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
        # Always use forward slashes, even on Windows.
        # Don't forget to use absolute paths, not relative paths.
        'C:/p2/socialscripter/www/socialscripter_django/templates',
    )

    CACHE_BACKEND = 'dummy:///'    
    #CACHE_BACKEND = 'locmem:///'
    #CACHE_BACKEND = 'file://c:/d/djangocache'
    #CACHE_BACKEND = 'db://djangocache?timeout=3600'
    DEBUG = True
    
else:
    
    #
    # REMOTE / PRODUCTION
    #
    
    SITE_ID = 2
    MEDIA_ROOT = '/home/tbak/p/socialscripter/www/media/'
    MEDIA_URL = 'http://media.socialscripter.com/'
    TEMPLATE_DIRS = ( '/home/tbak/p/socialscripter/www/socialscripter_django/templates' )
    
    # TB - Use memcached instead! Otherwise a big DB gets generated, which
    # results in big backup files.
    #CACHE_BACKEND = 'db://djangocache?timeout=3600'
    CACHE_BACKEND = 'memcached://127.0.0.1:11211/'  
    DEBUG = False
    #DEBUG = True


TEMPLATE_DEBUG = DEBUG

# On the local machine email is disabled, and emailing myself is retarded.
# This also greatly slows down displaying the page. 
if DEBUG == False and os.environ['DJANGOTEST_SERVER_LOCATION'] == 'remote':
    SEND_BROKEN_LINK_EMAILS = True


    

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
# ADMIN_MEDIA_PREFIX = '/media/'
ADMIN_MEDIA_PREFIX = MEDIA_URL + 'admin/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'e&g5!%d87ebjp^=56g!&io!5)o_pzbk*akqdtq76vt#5dssvc-'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',  
)

if DEBUG:
    MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + (
        'socialscripter_django.debug_middleware.DebugFooter',
        'socialscripter_django.djangologging.middleware.LoggingMiddleware',
        #'djangotest.profile_middleware.ProfileMiddleware',
    )   

# Used by djangologging
INTERNAL_IPS = ('127.0.0.1',)
# LOGGING_LOG_SQL = True # Doesn't work. Gives error: 'NoneType' object is unsubscriptable

ROOT_URLCONF = 'socialscripter_django.urls'

# Cache all anonymous non-post pages for 1 hour
CACHE_MIDDLEWARE_SECONDS = 60*60
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True
CACHE_MIDDLEWARE_KEY_PREFIX = 'socialscripter'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'socialscripter_django.main',
    'captcha',
    'compress',
    'django_evolution',
)

LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = '/myscripts'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    
    # This gives me request.path in the context
    # The ones on the top I snagged from django global settings... Is there no way to just append tihs one?
    'django.core.context_processors.request',
)

# CAPTCHA_FONT_PATH = 'fonts/Vera.ttf'
CAPTCHA_FONT_SIZE = 36
CAPTCHA_LETTER_ROTATION = (-15,15)
CAPTCHA_FOREGROUND_COLOR = '#880000'
#CAPTCHA_NOISE_FUNCTIONS = ('captcha.helpers.noise_arcs', 'captcha.helpers.noise_dots')
CAPTCHA_NOISE_FUNCTIONS = ('captcha.helpers.noise_dots',)


# Django-compress

COMPRESS = True
COMPRESS_VERSION = True
COMPRESS_CSS_FILTERS = None



COMPRESS_CSS = {
                
    'group_main': {
        'source_filenames': ('socialscripter.css', 'lib/datatables/datatables.css', ),
        'output_filename': 'compressed/group_main.?.css',        
    },
    
    'group_edit_script': {
        'source_filenames': ('lib/jquery.alerts-1.1/jquery.alerts.css', 'lib/ptTimeSelect/jquery.ptTimeSelect.css', 'lib/ptTimeSelect/timepicker_jquery-ui-1.7.1.custom.css' ),
        'output_filename': 'compressed/group_edit_script.?.css',        
    },

}

COMPRESS_JS = {
    
    'group_main': {
        'source_filenames': ('lib/jquery-1.4.min.js', 'lib/datatables/jquery.dataTables.min.js', 
'lib/datatables/datatables_extra.js', ),
        'output_filename': 'compressed/group_main.?.js',
    },
    
    'group_edit_script': {
        'source_filenames': ('lib/CodeMirror-0.65/js/codemirror.js', 
'lib/jquery.alerts-1.1/jquery.alerts.js', 'lib/ptTimeSelect/jquery.ptTimeSelect.js' ),
        'output_filename': 'compressed/group_edit_script.?.js',
    },

}
