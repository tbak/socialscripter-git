# Copyright Tom Bak

import string
import random

def clear(s):
    """Delete everything. Returns an empty string"""
    return ''

def uppercase(s):
    """Convert all letters to uppercase"""
    return s.upper()

def capitalize(s):
    """Capitalize the first letter of the first word"""
    return s.capitalize()

def reverse_letters(s):
    """Print all letters backwards"""
    new = []
    for l in s:
        new.insert(0,l)
    return ''.join(new)
        
def reverse_words(s):
    """For each word, write its letters backwards"""
    new = []
    for word in s.split(" "):
        new.insert(0,word)
    return ' '.join(new)

def piglatin(s):
    """Convert all words to piglatin: ellohay orldway"""
    new = []
    for word in s.split(" "):
        
        # Strip away punctuation from the end, and then put it back afterwards
        punc = None
        if word[-1] in string.punctuation:
            punc = word[-1]
            word = word[0:-1]
        
        if word[0] in 'aeiou':
            word = word + "ay"
        else:
            word = word[1:] + word[0] + "ay"
        
        if punc is not None:
            word = word + punc
            
        new.append( word )
        
    return ' '.join(new)

def common_misspell(s):
    """Applies common misspelling mistakes. "ee"->"ea", "ie"->"ei", 2 repeated letters to 1, etc."""


    """
    # Drop a repeated letter
    last_letter = s[0]
    for pos in range(1,len(s)-1):
        if s[pos] == last_letter:
            new = s[0:pos] + s[pos+1:]
            return new
        last_letter = s[pos]
    """    
    
    # Try to substitute one sequence of letters with another
    rules = [ ("ee","ea"), ("ea","ee"), ("ei","ie"), ("ie","ei"), ("ia","ai"), ("ai","ia"), (" I ", " i "), ("it's","its"), ("its","it's"), ("It's","Its"), ("Its","It's"), ("their","they're"), ("Their","They're"), ("they're","their"), ("They're","Their"), ("sc","s"), ("io","oi"), ("oi","io") ]
    
    start_rule = random.randrange(0,len(rules)) 
    for attempt in range(len(rules)):
        rule = rules[ (start_rule+attempt)%len(rules) ]
        pos = s.find(rule[0])
        if pos >= 0:
            #new = str(pos)                        
            # new = s[0:pos] + "(" + rule[0] + "-" + rule[1] + s[pos+len(rule[0]):]
            new = s[0:pos] + rule[1] + s[pos+len(rule[0]):]
            return new
        
    return s
        

def misspell(s):
    """Replace a single letter with another random letter. Preserves case."""
    start_pos = random.randrange(0,len(s))
    
    for attempt in range(5):
        pos = (start_pos + attempt) % len(s)
        if s[pos] in string.ascii_lowercase:
            replacement = string.ascii_lowercase[random.randrange(0,len(string.ascii_lowercase))]
        elif s[pos] in string.ascii_uppercase:
            replacement = string.ascii_uppercase[random.randrange(0,len(string.ascii_uppercase))]
        else:
            continue
                       
        new = s[0:pos] + replacement + s[pos+1:]
        return new
    
    # Couldn't find a normal letter???
    return s

def swap_neighbors(s):
    """Swap two random letters that are next to each other"""
    
    # don't include the last letter in random choice since we swap with +1
    start_pos = random.randrange(0,len(s)-1)
    
    for attempt in range(5):
        pos = (start_pos + attempt) % len(s)
        if s[pos] in string.ascii_letters and s[ (pos+1)%len(s) ] in string.ascii_letters:    
            new = s[0:pos] + s[pos+1] + s[pos] + s[pos+2:]
            return new
    
    # Couldn't find two letters next to each other?    
    return s

def drop_letter(s):
    """Remove a single random letter (or punctuation, spaces, etc)"""
    pos = random.randrange(0,len(s))
    new = s[0:pos] + s[pos+1:]
    return new

def repeat_letter(s):
    """Select a single random letter (or punctuation, spaces, etc) and repeat it. Hello->Heello"""
    pos = random.randrange(0,len(s))
    new = s[0:pos+1] + s[pos] + s[pos+1:]
    return new

def shuffle_words(s):
    """Print all of the words in random order"""
    new = []
    for word in s.split(" "):
        lword = list(word)
        random.shuffle(lword)
        new.append( "".join(lword) )
    return ' '.join(new)    

def test_parameter(s,param):
    """Doesn't do anything"""
    return s + param

def test_hidden_filter():
    """I am a hidden replacer! I should not be visible!"""
    return "FOO"

# These replacers will not show up in the help doc... but are still usable...
secret_methods = [test_hidden_filter]    

if __name__ == '__main__':
    
    tests = [("reverse_letters('foo')", "oof"),
             ("reverse_words('foo bar')", "bar foo"),
             ("reverse_words('foo bar qux')", "qux bar foo"),
             ("uppercase('foo')", "FOO"),
             ("capitalize('foo bar')", "Foo bar"),
             ("piglatin('foo bar')", "oofay arbay"),
             ("piglatin('open foo bar')", "openay oofay arbay"),
             ("misspell('hello')", None),
             ("misspell('hello')", None),
             ("drop_letter('hello')", None),
             ("drop_letter('hello')", None),
             ("shuffle_words('hello world')", None),
             ("swap_neighbors('hello world')", None),
             ("swap_neighbors('hello world')", None),
            ]
    
    for test in tests:        
        result = eval(test[0])
        if test[1] is not None:
            print test[0] + " : " + result + " == " + test[1] + " ? " + str(result == test[1])
            if result != test[1]:
                print "********************"
        else:
            print test[0] + " : " + result            
            
    exit()    