# Copyright Tom Bak

# TB TODO - The day/month/year funcs need to take into account the script timezone offset???

import datetime
import random
import logging
import time

# How the heck am I gonna get access to the scripts and misc settings???
# Maybe I must pass them in somehow??? (and then this module can just be ignorant about them)
# Ahhh but I need to make the database calls. How am I gonna do that???
# Create the replacers elsewhere, and then dynamically add them to this module?
# I DON'T KNOW!!!
#import socialscripter_django.settings

def get_datetime_localtz(script):
    now = datetime.datetime.now()
    secs = time.mktime(now.timetuple())
    
    if script is not None:
         secs += script.localtz_utc_offset_secs
    
    return datetime.datetime( *time.gmtime(secs)[0:6])


def today_day_of_week(script=None, **kwargs):
    """Return the day of the week that today is: Monday, Tuesday, etc"""   
    day_of_week = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')
    return day_of_week[datetime.date.weekday(get_datetime_localtz(script))]

def today_day(script=None, **kwargs):
    """Return the day that today is: 1, 4, 16, etc"""    
    return str(get_datetime_localtz(script).day)

def today_month(script=None, **kwargs):
    """Return the month that today is: January, February, etc"""
    months = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')
    return months[get_datetime_localtz(script).month-1]

def today_year(script=None, **kwargs):
    """Return the year that today is: 2009, 2010, etc"""    
    return str(get_datetime_localtz(script).year)

def random_number(script=None, **kwargs):
    """Return a single number from 0 to 9"""
    return str( random.randint(0, 9) )

def twitter_random_follower(script=None, **kwargs):
    """Return the username of a random twitter follower: joe_bob, frank2010, etc. 
    This requires that your twitter account be properly authenticated, and that you actually have followers.
    You probably want to add an @ in front of this, like so: @[_random_follower] to get @joe_bob.
    """
    
    from socialscripter_django.main.models import TwitterFollower
    
    #twitter_followers = TwitterFollower.objects.all().filter(script__exact = script )    
    # This is faster than order by random    
    num_followers = TwitterFollower.objects.all().filter(script__exact = script ).count()
    if num_followers is 0:
        return "???"
    
    random_follower = TwitterFollower.objects.all().filter(script__exact = script )[random.randint(0, num_followers-1)]
    return random_follower.follower_username


def twitter_trend(**kwargs):
    """Return a random top 10 twitter trend. Any hash tags have been removed. "econony", "britney spears", etc."""

    from socialscripter_django.main.models import TwitterTrend    
    
    # This is faster than order by random    
    num_trends = TwitterTrend.objects.all().filter(enabled__exact = True ).count()
    if num_trends is 0:
        return "???"
    
    random_trend = TwitterTrend.objects.all().filter(enabled__exact = True )[random.randint(0, num_trends-1)]
    return random_trend.trend     
        

def test_hidden_replacer(**kwargs):
    """I am a hidden replacer! I should not be visible!"""
    return "FOO"

# These replacers will not show up in the help doc... but are still usable...
secret_methods = [get_datetime_localtz, test_hidden_replacer]

if __name__ == '__main__':
    
    import sys
    sys.path.append('C:/p2/socialscripter/socialscripter_django')
    import settings
    
    tests = [("today_day_of_week()", None),             
             ("today_day()", None),
             ("today_month()", None),
             ("today_year()", None),
             ("twitter_trend()", None),
             ("twitter_trend()", None),
            ]
    
    for test in tests:        
        result = eval(test[0])
        if test[1] is not None:
            print test[0] + " : " + result + " == " + test[1] + " ? " + str(result == test[1])
            if result != test[1]:
                print "********************"
        else:
            print test[0] + " : " + result            
            
    exit()    