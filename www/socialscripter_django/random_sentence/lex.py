# Copyright Tom Bak

from random import shuffle, randrange
import random
import logging
import re

import filters, replacers

class Category:  
   
    def __init__(self, name, line_num):
        """ line_num is used so that a category error can
        refer back to the line number in the script file """        
        self.name = name
        self.line_num = line_num
        self.replacements = []
        self.filters = []
        self.vars = {}
        
    def add_replacement(self, r, prob=1):
        if len(r) == 0:
            return
        
        # Multiply all probabilities by 100 so that we can use
        # floats but that just use randrange()
        self.replacements.append( (r,prob*100) )
        
    def create_sample(self):
        self.add_replacement("hello")
        self.add_replacement("yo",0.5)            
        
    def get_random(self):
        selected_text = None
        current_prob = 0
        
        for r in self.replacements:
            # TB TODO - Does this work with floats?
            # TB TODO - This is not working right!
            # Now it's working with the *100 fix?            
            if randrange(0,current_prob+r[1]) <= r[1]:
                selected_text = r[0]
                
            current_prob = current_prob + r[1]
          
          
        if selected_text == None:
            # TB TODO - Print will blow up when run from django
            print "Nothing was selected?"
            print self.name
            print self.replacements  
        
        return selected_text

class Lex():

    # +? is nongreedy. It'll consume the first <xxx> instead of "<xxx>dsfdsf<xxx>" if regular greedy + were used.
    re_catrefs = re.compile(r'\[([\*\w]+?)\]')    
    
    # {stuff}
    re_systemvars = re.compile(r'{(\w+?)}')
    
    # {  filter :   filter_name , param1,  param2   }
    #re_filter = re.compile(r'{[\W]*filter[\W]*:[\W]*(\w+)[,(\w+)]*[\W]*}')
    #re_filter = re.compile(r'{[\W]*filter[\W]*:[\W]*(\w+)([\W]*,[\W]*(\w+))*[\W]*}')
    #re_filter = re.compile(r'{[\W]*filter[\W]*:[\W]*(\w+)[\s]*:?([\w,"\']*)[\W]*}')
    re_filter = re.compile(r'\{[\W]*filter[\W]*:[\W]*(\w+)[\s]*(:[\s]*([\w,"\']+))?\}')
    
    #re_var = re.compile(r'\{[\W]*var[\W]*:[\W]*(\w+)[\s]*=[\s]*([.]+)[\s]*\}')
    re_var = re.compile( r'\{[\s]*var[\s]*:[\s]*(\w+)[\s]*=[\s]*(.+)[\s]*\}' )

    def __init__( self, script=None ):
        self.errors = []
        self.warnings = []
        self.cats = {}
        self.debug_enabled = False
        self.debug_buffer = ""
        self.runtime_vars = {}
        self.script = script
    
    def load_lex(self, filename):
        lex_file = open(filename, 'r')
        self.build_lex(lex_file)
    
        
    def build_lex(self, input):

        cat = None            
        line_num = 0                     
    
        for line in input:
            
            line_num = line_num + 1
            
            if len(line.lstrip().rstrip()) == 0:
                continue
            
            if line[0] == '#':
                
                # START OF NEW CATEGORY
                                
                name = line[1:].lstrip().rstrip()
                
                if name in self.cats:
                    self.errors.append( (line_num, "Category \"%s\" has already been declared on line %d."%(name,self.cats[name].line_num)) )                    
                
                cat = Category(name, line_num)
                self.cats[name] = cat
            
            elif line[0] == '/' and line[1] == '/':
                
                # COMMENT
                continue
            
            elif line[0] == '{':    
                
                # FILTER
                
                if cat == None:                    
                    self.errors.append( (line_num, "This system command has no category associated with it. Put filters after your #category line.") )
                
                else:
                    
                    # Category vars
                    match = Lex.re_var.search(line)
                    if match:
                        #print 'VAR1 ' + match.group(1)
                        #print 'VAR2 ' + match.group(2)
                        var_name = match.group(1)
                        var_value = match.group(2)
                        
                        if var_name == cat.vars:
                            self.errors.append( (line_num, "Var with same name added to a category") )
                            
                        cat.vars[match.group(1)] = match.group(2) 
                        continue
                        pass
                    
                    
                    
                    
                    
                    
                    
                    # Parse out the name of the filter
                    match = Lex.re_filter.search(line)
                    #print dir(match)
                    #print len(match.groups())
                    if match is None:
                        self.errors.append( (line_num, "This system command is formated improperly. It should look like {filter:filter_name}") )
                    else:
                        #print 'ALL: ' + match.group(0)
                        
                        filter_name = match.group(1)
                        #print 'FILTER: ' + filter_name                                               
                                                
                        params = []
                        if match.group(2) is not None:
                            #print 'PARAMS: ' + match.group(2)                                                            
                            #print 'PARAMS2: ' + match.group(3)
                            params = match.group(3).split(",")
                            
                        # Test calling this function to make sure the params are OK
                        # Otherwise catch the resulting exception and create an error
                        try:                            
                            func = getattr(filters, filter_name)                            
                            func("Test string", *params)
                            
                            # Add the filter to this category
                            cat.filters.append( (func, params) )
                        except AttributeError, e:
                            self.errors.append( (line_num, "Filter '%s' does not exist."%filter_name) )                            
                        except TypeError, e:
                            self.errors.append( (line_num, "Filter '%s' is being passed the wrong number of parameters."%filter_name) )                            
                        except Exception, e:
                            self.errors.append( (line_num, "Filter '%s' error: %s"%(filter_name,str(e))) )
                                    
                
            else:
                
                # REPLACEMENT STRING
            
                if cat == None:
                    self.errors.append( (line_num, "Text has no category associated with it") )
            
                else:             
                
                    # Look for probability token
                    string_end = len(line)
                    probability = 1
                    prob_start = line.find('{')
                    prob_end = line.find('}')
                    #if prob_start != -1 and prob_end == len(line)-1 and prob_start < prob_end:
                    if prob_start != -1:                                                
                        # TB TODO - Extract float from the value
                        try:
                            probability = float(line[prob_start+1:prob_end])
                        except(ValueError):
                            self.errors.append((line_num, "Probability value inside of {} is not a valid number."))
                            
                        string_end = prob_start
                        #logging.debug(probability)               
                        #logging.debug(line[0:string_end])                    
                    
                    cat.add_replacement(line[0:string_end].lstrip().rstrip(), probability)
                
            #print line.lstrip().rstrip()
            
        self.check_lex()           
        
    
    def check_lex(self):
        """ Returns errors,warnings
        Errors and warnings are in the form [(line_num,text),...]
        
        Checks for:
        replacement refers to cat that doesn't exist (error)
        "start" does not exist (error)
        Mismatched <> (error)
        Empty cat (error)
        Two cats with same name(error)
        cat is not used anywhere (warning)         
        """                
        
        # Does start exist?
        if 'start' not in self.cats:
            self.errors.append( (0, "\"start\" category could not be found.") )
        
        # Category has not replacement lines
        for cat in self.cats.values():    
            if len(cat.replacements) == 0:
                self.errors.append( (cat.line_num, "Category \"" + cat.name + "\" does not have any replacement lines (it's empty).") )
                    
            
        # Get a dict with all category names that we are referencing in the text
        # { name : [usage0_line_num, usage1_line_num,etc], ... }
        used_cats = {}
        used_cats['start'] = [0]
        
        for cat in self.cats.values():
            for r in cat.replacements:
                # Add all used cats to dict                
                # Iterating over a simple regex match would do this nicely... too bad I have NO INTERNET!!!
                                
                #if re_catrefs.search(r[0]) is not None:
                for match in Lex.re_catrefs.findall(r[0]):
                    
                    if match[0] == '*':
                        # var replacer!
                        # TB TODO - Any way to check if a [*cat] is being used somewhere that potentially will not be valid?
                        # Yeah... Do a DFS of all possible routes. Collect the generated vars, and only propagate those vars that are the same for all possible children.
                        # Given those vars, continue with DFS. If a var is referenced that is not in this limited list, the var reference is not 100% safe. 
                        pass
                        
                    elif match[0] == '_':
                        # print 'SYSTEM REPLACER!'
                        
                        # Test that this function exists
                        # TB TODO - Parameters to the system funcs???
                        # I guess when I figure out what to use them for???
                        try:
                            # Remove the initial underscore, since they are not defined that way in code
                            func = getattr(replacers, match[1:])                            
                            func(script=self.script)                            
                        except AttributeError, e:
                            self.errors.append( (cat.line_num, "Built-in replacer '%s' does not exist."%match) )
                        except TypeError, e:
                            self.errors.append( (cat.line_num, "Built-in replacer '%s' is being passed the wrong number of parameters."%match) )                            
                        except Exception, e:
                            self.errors.append( (cat.line_num, "Built-in replacer '%s' error: %s"%(match,str(e))) )
                        
                    else:
                        
                        # It's a regular category that is being referenced within the script.
                        # So add it so that we can later check that this category is defined somewhere.                                        
                        used_cats.setdefault(match,[]).append( cat.line_num )
       
        # used_cats = {'start':(5), 'omega':(12,15)}
        
        # Category is not used anywhere?
        for k,v in self.cats.items():
            
            # Skip the system cats that are auto-added to the lex, but don't need to be used
            if k[0] == "_":
                continue
            
            if k not in used_cats:
                # TB TODO - Put this back once used_cats works!!! Right now it just spams with crap
                self.warnings.append( (v.line_num, "Category \"" + v.name + "\" is not used anywhere.") )
                #pass
                
        # Text is referring to a category that doesn't exist
        for k,v in used_cats.items():
            if k not in self.cats:
                for line_num in v:
                    self.errors.append( (line_num, "Nonexisting category \"" + k + "\" is being referenced.") )            
                
        self.warnings.sort()
        self.errors.sort()     
    
    def add_debug(self, s, depth=0):
        self.debug_buffer = self.debug_buffer + ' ' * depth + s + '\n'
  
    def expand_cat(self,cat,depth):
        """Apply a replacement from this category to the given text. Then apply filters after all subcats have been expanded"""

        self.runtime_num_expansions = self.runtime_num_expansions + 1
        if self.runtime_num_expansions > 200:
            
            if self.debug_enabled:
                self.add_debug( '***** Error: Too many category expansions.' )
                        
            return ""
                
        
        # Apply replacement
        text = cat.get_random()
        
        if self.debug_enabled:
            self.add_debug('[' + cat.name + ']', depth)
            
        depth = depth + 4
        
        
        # Add all vars associated with this category
        for k,v in cat.vars.items():
            self.runtime_vars[k] = v
            if self.debug_enabled:
                self.add_debug( 'var: ' + k + ' = ' + v , depth )
                
        
        if self.debug_enabled:             
            self.add_debug('"' + text + '"', depth)    
            
        depth = depth + 4                    


        
        # Look for more
        # Call expand_cat for each one
        expand_more = True
        while expand_more:
            match = Lex.re_catrefs.search(text)
            if match is not None:                
                new_cat_name = match.group(1)
                
                if new_cat_name[0] == '!':
                    
                    # cat NOT variable reference (pick anything BUT what was chosen
                    # TB TODO: hmm... how to do this? It'll get confused if you try to ! a higher level category.
                    # Would it then have to branch to another category right off the bat, or does it just require ANY other kind of input... hmmmm....
                    # The particular trail that resulted in that output would need to be blocked somehow... but everything else is legit... hmmmm...
                    pass
                    
                elif new_cat_name[0] == '*':
                    
                    # cat variable reference!
                                        
                    # Remove intial * from cat name
                    n = new_cat_name[1:]
                    if n not in self.runtime_vars:
                        self.add_debug('[' + new_cat_name + '] could not be expanded.', depth-4)
                        new_cat_text = new_cat_name
                    
                    else:
                        new_cat_text = self.runtime_vars[n]
                    
                elif new_cat_name[0] == '_':
                    # Builtin replacer
                    # Remove the initial underscore, since they are not defined that way in code
                    func = getattr(replacers, new_cat_name[1:])                            
                    new_cat_text = func(script=self.script)                             
                else:
                    new_cat_text = self.expand_cat( self.cats[new_cat_name], depth )
                
                text = text[0:match.start()] + new_cat_text + text[match.end():]
                                
                if self.debug_enabled:            
                    self.add_debug('"' + text + '"', depth-4)
            
            else:
                expand_more = False

        depth = depth - 4
        
        # We now have nothing but normal text. We can apply the filters!        
        for filter in cat.filters:
            text = filter[0](text, *filter[1])
            
            if self.debug_enabled:            
                self.add_debug('"' + text + '"' + ' (filter ' + filter[0].__name__ + ')', depth)

        # Store this value as a runtime var, so that we can use it again as [*cat]
        self.runtime_vars[cat.name] = text

        return text

    
    def generate_sentence(self):
        self.runtime_num_expansions = 0
        self.runtime_vars = {}        
        result = self.expand_cat( self.cats['start'], 0 )
        
        if self.debug_enabled:            
            self.add_debug('')
            
        return result



if __name__ == '__main__':
    
    print 'RANDOM SENTENCE'

    lex = Lex()
    lex.debug_enabled = True
    #lex.load_lex('c:/p2/socialscripter/media/scripts/test.txt')    
    lex.load_lex('c:/p2/socialscripter/media/scripts/midget.txt')
    
    if len(lex.warnings) > 0:
        print "Warnings:"
        for warning in lex.warnings:
            print "Line %d: %s" % (warning[0], warning[1])
            
    if len(lex.errors) > 0:
        print "Errors:"
        for error in lex.errors:
            print "Line %d: %s" % (error[0], error[1])
        exit(-1)
    
    output = lex.generate_sentence()

    if lex.debug_enabled:    
        print "debug:\n" + lex.debug_buffer
    
    print "output:\n" + output        
    