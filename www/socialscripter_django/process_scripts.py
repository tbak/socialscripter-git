#!/usr/bin/env python

import django
from django.core.management import setup_environ
import random
import datetime
import time
import twitter
import facebook
import oauthtwitter
from random_sentence.lex import Lex
import simplejson
from urllib2 import HTTPError, urlopen
import os

# 5 = everything
# 3 = things that have changed
# 2 = Creating new entries, new posts, etc
# 1 = start of run + weird things
# 0 = nothing
verbosity = 99

# Options parsing needs to be at the top of this file so that os.environ can be set when settings is imported.
if __name__ == "__main__":
    
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-r", "--remote", action="store_const", const="remote", dest="server_location", help="Use the remote applicaton twitter settings")
    parser.add_option("-l", "--local", action="store_const", const="local", default="local", dest="server_location", help="Use the local callback twitter settings")
    parser.add_option("-d", "--daily", action="store_true", dest="do_daily_update", default=False, help="Perform the daily checks: inspect twitter new followers,friends,etc")
    parser.add_option("-v", "--verbose", action="store", dest="verbosity", default="99" )

    (options, args) = parser.parse_args()
    os.environ['DJANGOTEST_SERVER_LOCATION'] = options.server_location
    verbosity = int(options.verbosity)


try:
    import settings # Assumed to be in the same directory.
except ImportError:
    import sys
    sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.exit(1)

setup_environ(settings)
from main.models import Script, History, ScheduledPost, TwitterFollower, TwitterTrend
from main.views import twitter_consumer_key, twitter_consumer_secret, facebook_api_key, facebook_secret, get_now_utc_datetime
from django.core.mail import send_mail
from django.db.models import Q

# Calculate when this script needs to update and start spawning posts...
def get_scheduler_next_update_utc(script):
    now_utc = get_now_utc_datetime()
    now_localtz = now_utc + datetime.timedelta( seconds=script.localtz_utc_offset_secs )
    midnight_localtz = datetime.datetime(now_localtz.year, now_localtz.month, now_localtz.day, 0, 0 )
    tomorrow_midnight_localtz = midnight_localtz + datetime.timedelta( days=1 )
    tomorrow_midnight_utc = tomorrow_midnight_localtz - datetime.timedelta( seconds=script.localtz_utc_offset_secs )
    return tomorrow_midnight_utc


def create_scheduled_post(script, midnight_localtz, hour, min, social_service):

    if verbosity >= 3: print "> create_scheduled_post(" + social_service + ")"
                
    post_time_localtz = midnight_localtz + datetime.timedelta( hours=hour, minutes=min )
    if verbosity >= 4: print "post time localtz: " + str(post_time_localtz)
    if verbosity >= 4: print "offset: " + str(script.localtz_utc_offset_secs)
    # The offset is negative since we are going BACK to utc time!
    post_time_utc = post_time_localtz - datetime.timedelta( seconds=script.localtz_utc_offset_secs )
    if verbosity >= 4: print "post time utc: " + str(post_time_utc)
    
    # If this post is scheduled for BEFORE now, ignore it.
    # This can happen if we are doing an instant scheduler run because the user changed some settings.
    # The scheduler will then not necessarily run at midnight, so it's possible that some posts
    # will have to wait a day!
    now_utc = get_now_utc_datetime()
    if post_time_utc < now_utc:        
        if verbosity >= 3: print "This post takes place in the past. Skipping..."
        return

    scheduled_post = ScheduledPost()
    scheduled_post.script = script
    scheduled_post.social_service = social_service
    scheduled_post.enabled = True
    scheduled_post.posted = False
    
    # If i record this in UTC time zone, then the check scheduled posts code
    # can run much faster. Doesn't need to constantly get the time for different
    # time zones.
    scheduled_post.post_time_utc = post_time_utc
    scheduled_post.save() 

def create_scheduled_direct_message_twitter(script, target_user, mins_from_now):
    scheduled_post = ScheduledPost()
    scheduled_post.script = script
    scheduled_post.social_service = 'D'
    scheduled_post.target_user = target_user
    scheduled_post.enabled = True
    scheduled_post.posted = False
    scheduled_post.post_time_utc = get_now_utc_datetime() + datetime.timedelta( minutes = mins_from_now )
    scheduled_post.save() 
    

def create_scheduled_posts_social_service(script, script_timing, social_service ):
        
    # Remove any old lingering scheduled posts for the social service we are posting to    
    scheduled_post_list = ScheduledPost.objects.all().filter(script__id__exact = script.id ).filter(enabled__exact = True).filter(posted__exact = False).filter( Q(social_service__exact=social_service) )   
    for scheduled_post in scheduled_post_list:
        scheduled_post.enabled = False
        scheduled_post.save()
        
    if script_timing.frequency == 0 or script_timing.frequency == 4 or script_timing.frequency == 5:
        return        
    
    now_localtz = get_now_utc_datetime() + datetime.timedelta( seconds=script.localtz_utc_offset_secs )   
        
    if verbosity >= 4: print "now (local tz): " + str(now_localtz)    
    
    midnight_localtz = datetime.datetime(now_localtz.year, now_localtz.month, now_localtz.day, 0, 0 )
    if verbosity >= 4: print "midnight_localtz: " + str(midnight_localtz)
    
    # This is midnight for the local tz, represented in utc time
    midnight_localtz_utc = midnight_localtz - datetime.timedelta( seconds=script.localtz_utc_offset_secs )  
    
    # Create new posts according to script rules
    if script_timing.frequency == 1 or script_timing.frequency == 2:
        
        # Daytime or anytime, pick a number of times to post and then schedule them
        # The only difference between these two is the range of allowable hours permitted,
        # and which range variables to use.
        if script_timing.frequency == 1:
            num_posts = random.randrange(script_timing.wakingtime_range_min, script_timing.wakingtime_range_max+1)
            hour_range_min = 9
            hour_range_max = 21
        else:
            num_posts = random.randrange(script_timing.anytime_range_min, script_timing.anytime_range_max+1)
            hour_range_min = 0
            hour_range_max = 24

        # Count the number of posts we have already made today, and subtract those.
        # This can happen if we're running an immediate scheduler update (not at midnite), and some
        # previously scheduled posts have already gotten a chance to get posted...            
        num_posts_already = ScheduledPost.objects.all().filter(script__exact = script ).filter(posted__exact = True).filter(enabled__exact = True).filter(post_time_utc__gt = midnight_localtz_utc).filter( Q(social_service__exact=social_service) ).count()
        if verbosity >= 4: print "num already posted today = " + str(num_posts_already)        
        num_posts -= num_posts_already
        
        for i in range(num_posts):            
            hour = random.randrange(hour_range_min,hour_range_max)
            min = random.randrange(0,60)
            create_scheduled_post(script, midnight_localtz, hour, min, social_service)             
    
    elif script_timing.frequency == 3:
        # Specific time
        # TB TODO - Randomize it a little bit?
        create_scheduled_post(script, midnight_localtz, script_timing.exacttime.hour, script_timing.exacttime.minute, social_service)
        pass

def create_scheduled_posts(script):
    if verbosity >= 2: print ">> Creating scheduled posts for: " + script.name
    
    # Only bother to create scheduled posts if the dude's been authenticated
    if script.twitter_account is not None:        
       create_scheduled_posts_social_service(script, script.twitter_timing, 'T')
       
    if script.facebook_account is not None:    
        create_scheduled_posts_social_service(script, script.facebook_timing, 'F')

    script.scheduler_next_update_utc = get_scheduler_next_update_utc(script)
    script.save()

def post_twitter(script, msg):
        
    if verbosity >= 2: print "> post_twitter"
    
    try:
        twitter = oauthtwitter.OAuthApi(twitter_consumer_key, twitter_consumer_secret, oauthtwitter.oauth.OAuthToken.from_string(script.twitter_account.oauth_access_token) )    
        twitter.PostUpdate(msg)
        
        script.twitter_num_posts = script.twitter_num_posts + 1
        script.save()    
        return True
    
    except Exception, e:
        if verbosity >= 1: print "post twitter exception: " + str(e)
        return False
    
def post_facebook(script, msg):
    if verbosity >= 2: print "> post_facebook"
    
    try:
        fb = facebook.Facebook(facebook_api_key, facebook_secret)
        
        fb.uid = script.facebook_account.uid
        fb.auth_token = script.facebook_account.session_key       
        fb.session_key = script.facebook_account.session_key

        #friends = fb.friends.get()
        #print friends
        
        # This is probably better than publish. This way it doesn't say that it's from SocialScripter...
        r = fb.users.setStatus( status = msg, clear=False, uid = fb.uid, status_includes_verb=True ) 
        if r is True:        
            script.facebook_num_posts = script.facebook_num_posts + 1
            script.save()    
        """
        # Try publishing something
        message = "Test Message with Picture"    
        attachment = simplejson.dumps({ 'media': [ {'type': 'image',
                                                    'src': 'http://www.topnews.in/usa/files/megan-fox.jpg',
                                                    'href': 'http://www.google.com'}]})
        action_links = simplejson.dumps([{'text': 'lalala', 'href': 'http://myurl.com/'}])
        
        # This Works!
        #r = fb.stream.publish( message = message, attachment = attachment, action_links = action_links, uid = fb.uid) 
        #logging.debug(r)
        """        
                
        return r
        
    except Exception, e:
        if verbosity >= 1: print "post facebook exception: " + str(e)
        return False
        

def send_direct_message_twitter( script, msg, target_user ):
    if verbosity >= 2: print "> send_direct_message_twitter to " + target_user
    try:
        twitter = oauthtwitter.OAuthApi(twitter_consumer_key, twitter_consumer_secret, oauthtwitter.oauth.OAuthToken.from_string(script.twitter_account.oauth_access_token) )    
        twitter.PostDirectMessage( target_user, msg )
        script.twitter_num_posts = script.twitter_num_posts + 1
        script.save()        
        return True
    
    except Exception, e:
        if verbosity >= 1: print "send_direct_message_exception: " + str(e)
        return False    

def post_scheduled_post(scheduled_post):
    
    # Generate a message
    # TB TODO - Would pickling the lex structure for each script be faster???
    
    if verbosity >= 2: print ">> post_scheduled_post: " + scheduled_post.script.name
        
    lex = Lex()
    lex.debug_enabled = False;
    lines = scheduled_post.script.script_data.splitlines()
    lex.build_lex(lines)        
    
    event = -1
    extra = None
    
    if len(lex.errors) == 0:
        msg = lex.generate_sentence()
        
        if scheduled_post.social_service == 'T':
            
            # If the dude has changed the post frequency to "never", don't post any previously
            # scheduled posts, fool!
            if scheduled_post.script.twitter_timing.frequency == 0:
                return
            
            event = 101
            result = post_twitter(scheduled_post.script, msg)
            if result is True: 
                event = 100
        
        elif scheduled_post.social_service == 'D':
            
            # If the dude has changed the post frequency to "never", don't post any previously
            # scheduled posts, fool!
            if scheduled_post.script.twitter_timing.frequency == 0:
                return
            
            event = 103
            extra = scheduled_post.target_user
            result = send_direct_message_twitter( scheduled_post.script, msg, scheduled_post.target_user )
            if result is True:
                event = 102                
        
        else:
            event = 201
            
            if scheduled_post.script.facebook_timing.frequency == 0:
                return
            
            result = post_facebook(scheduled_post.script, msg)
            if result is True:
                event = 200
    
        # If everthing's cool, then it's been posted. Otherwise we'll have to try again.
        # NOTE: If the account is screwed up it's just gonna keep failing!
        if result == True:
            scheduled_post.posted = True
            scheduled_post.save()                
            
    # Post whatever happened to history (either success for failure)
    history = History()
    history.script = scheduled_post.script
    history.user = scheduled_post.script.user
    history.event = event
    history.extra = extra
    history.save()

def create_friendship_twitter( twitter, name ):
    # TB - I hacked this because twitter.CreateFriendship is returning a bad request
    # According to http://code.google.com/p/oauth-python-twitter/issues/detail?id=8 this is a workaround
    
    try:
        url = 'http://twitter.com/friendships/create/%s.json' % name
        json = twitter._FetchUrl(url, post_data={'screen_name': name}, no_cache=True) 
        data = simplejson.loads(json)
        #twitter._CheckForTwitterError(data)
        #print User.NewFromJsonDict(data)
        return True
    
    except HTTPError, e:
        if e.code == 403:
            # "forbidden" was returend. That's probably because we are already following this person!
            if verbosity >= 2: print "FORBIDDEN!!! User already added?"
            return True
        else:
            if verbosity >= 1: print "Other error code! Skip!"
            return False

def check_new_followers_twitter( script, twitter ):
    all_followers = twitter.GetFollowers()
    all_followers = [ a.GetScreenName() for a in all_followers ]
    
    current_follower_list = TwitterFollower.objects.all()
    current_follower_list = [ a.follower_username for a in current_follower_list ]
    
    new_followers = [ a for a in all_followers if a not in current_follower_list ]        
    
    if verbosity >= 3: print "New followers: " + str(new_followers)
    
    # Any followers not in the followers list for this script should then follow this follower!
    # And potentially also send a direct message!        
    for new_follower in new_followers:
        
        if script.twitter_befriend_followers:
            if verbosity >= 2: print "Creating friendship with new follower: " + new_follower
                        
            result = create_friendship_twitter(twitter,new_follower)
            if result is False:
                # Something went wrong. Skip this and get back to it later.
                # If we don't skip, then the follower will be added to the twitterfollower list
                # and will never be actually added.
                continue
            
                            
        # Direct message to new follower?    
        if script.twitter_timing.frequency == 4:
            if verbosity >= 2: print 'Scheduling a direct message for ' + new_follower
            #create_scheduled_direct_message_twitter(script, new_follower, random.randrange(0,1440))
            # Wait up to 6 hours. (twitter update runs every 6 hours, so this'll cover all possible hours)
            create_scheduled_direct_message_twitter(script, new_follower, random.randrange(0,360))
            
        
        # Mark this follower as having been processed
        tf = TwitterFollower()
        tf.follower_username = new_follower
        tf.script = script
        tf.save()
        
def check_replies_twitter( script, twitter ):
    # TB TODO - Check for @ reply
    # If there is a new @ reply:
    #    Auto add (if enabled)
    #    post direct reply (if enabled)
    #    Add this @ reply to processed list
    
    since_id = script.twitter_last_reply_id   
    
    all_replies = twitter.GetReplies(since_id=since_id)    
    if verbosity >= 3: print "All replies: " + str(all_replies)
        
    for reply in all_replies:
        
        screen_name = reply.user.screen_name
        print str(reply.id)
        print reply.text
        print str(screen_name)        
                
        if script.twitter_befriend_replies:
            if verbosity >= 2: print "Creating friendship with new reply: " + screen_name
            result = create_friendship_twitter(twitter,screen_name)
            if result is False:
                # Something screwed up. Try processing again later.
                break
            
        # Direct message to new reply?    
        if script.twitter_timing.frequency == 5:
            if verbosity >= 2: print 'Scheduling a direct message for ' + screen_name
            create_scheduled_direct_message_twitter(script, screen_name, random.randrange(0,1440))
        
        if reply.id > since_id:    
            since_id = reply.id            
    
    # Remember the last ID we processed... So that we only get new replies. 
    script.twitter_last_reply_id = since_id
    script.save()

def update_twitter():
    # If there is a new follower:
    #     auto add (if enabled)
    #     post direct reply (if enabled)
    #     Add this follower to processed list
    script_list = Script.objects.all().filter(enabled__exact = True)
    #print script_list
    for script in script_list:
        
        if script.twitter_account is None:
            continue        
        
        if verbosity >= 3: print "update_twitter(), script = " + script.name

        try:
        
            twitter = oauthtwitter.OAuthApi(twitter_consumer_key, twitter_consumer_secret, oauthtwitter.oauth.OAuthToken.from_string(script.twitter_account.oauth_access_token) )
            check_new_followers_twitter(script, twitter)
            check_replies_twitter(script,twitter)
            
        except Exception, e:
            if verbosity >= 1: print "update_twitter exception: " + str(e)
            
         


def get_current_twitter_trends():
    if verbosity >= 3: print "Getting current twitter trends"
    try:
        # Get all the old trends
        # Mark them all as disabled AFTER we add the new trends
        # Otherwise we risk trends not being available for a little while
        old_trend_list = TwitterTrend.objects.all().filter(enabled__exact = True)
        old_trend_ids = [ a.id for a in old_trend_list ]
                 
        json = urlopen("http://search.twitter.com/trends/current.json?exclude=hashtags").read()
        data = simplejson.loads(json)        
        
        for k, v in data['trends'].iteritems():
            
            items = [ item['name'] for item in v ]
            if verbosity >= 4: print items            
                        
            for item in items:                                 
                trend = TwitterTrend()
                trend.trend = item.lstrip().rstrip()
                trend.save()
                
        for old_trend_id in old_trend_ids:
            old_trend = TwitterTrend.objects.get(id__exact=old_trend_id)
            old_trend.enabled = False
            old_trend.save()
                
    except HTTPError, e:
        if verbosity >= 1: print "get twitter trends error: " + str(e)        
        
    if verbosity >= 3: print 'Done getting trends'

def main():
    
    now = get_now_utc_datetime()
    if verbosity >= 1: print "##### START PROCESS_SCRIPTS at " + str(now)     
    
        
    if verbosity >= 5: print '*** START CREATE SCHEDULED POSTS'
    
    # Get the list of all scripts that are due for an update
    script_list = Script.objects.all().filter(enabled__exact = True).filter(scheduler_next_update_utc__lt = now)    
    
    for script in script_list:
        create_scheduled_posts(script)

    if verbosity >= 5: print '*** START POST SCHEDULED POSTS'    
        
    # Now iterate trough all scheduled posts (enabled and not already posted) and check if the now (utc) time is now past its scheduled time!!!    
    # TB TEMP! Make believe it's 20 hours later!
    #now = now + datetime.timedelta( hours = 20 )
    scheduled_post_list = ScheduledPost.objects.all().filter(post_time_utc__lt = now).filter(enabled__exact = True).filter(posted__exact = False)
    for scheduled_post in scheduled_post_list:
        post_scheduled_post(scheduled_post)



    # Only do this a few times a day?
    # If I move this into the create_scheduled_posts loop above I can do it once a day... but maybe
    # that's not often enough? It could be up to 2 days for a twitter reply to take place.
    if options.do_daily_update is True:
        if verbosity >= 1: print '*** START DAILY UPDATE STUFF'
        update_twitter()
                
        get_current_twitter_trends()
        
            
    if verbosity >= 5: print "Done! Now BUGGER OFF!"

if __name__ == "__main__":
    
    if verbosity > 10:
        print "temp test!"
        if options.server_location is "remote":
            print "REMOTE!"
            msg = "Test message"
            send_mail('*** SocialScripter process_scripts.py unhandled exception!', msg, "noreply_bugs@socialscripter.com", ["tom@socialscripter.com"], fail_silently=False)
    
    try:        
        main()
    except Exception, e:
        # Print an error and email myself with the traceback
        import traceback
        msg = "********** Unhandled fatal exception: " + str(e) + "\n"
        msg += traceback.format_exc()
        if verbosity >= 1: print msg
        if options.server_location is "remote":
            send_mail('*** SocialScripter process_scripts.py unhandled exception!', msg, "noreply_bugs@socialscripter.com", ["tom@socialscripter.com"], fail_silently=False)
