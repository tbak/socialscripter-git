import wx


class EditorFrame(wx.Frame):
    
    def OnNew(self,event):
        print "New!"
        
    def OnRun(self,event):
        print "Run!"
    
    def __init__(self):
        parent = None        
        wx.Frame.__init__(self, parent, -1, "Random Sentence", wx.DefaultPosition, (400,300))
        
        menu = wx.Menu()
        menu.Append(1001, "New")
        self.Bind(wx.EVT_MENU, self.OnNew, id=1001)
        menubar = wx.MenuBar()
        menubar.Append(menu, "File")
        self.SetMenuBar(menubar)
        
        tbflags = ( wx.TB_HORIZONTAL | wx.NO_BORDER | wx.TB_FLAT )
        tb = self.CreateToolBar( tbflags )
        
        tsize = (24,24)
        new_bmp =  wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_TOOLBAR, tsize)
        open_bmp = wx.ArtProvider.GetBitmap(wx.ART_FILE_OPEN, wx.ART_TOOLBAR, tsize)
        tb.SetToolBitmapSize(tsize)

        tb.AddLabelTool(11, "Run", open_bmp, shortHelp="Run", longHelp="Long help for 'Run'")
        self.Bind(wx.EVT_TOOL, self.OnRun, id=11)
        
        tb.AddSeparator()
                
        tb.AddLabelTool(10, "New", new_bmp, shortHelp="New", longHelp="Long help for 'New'")
        self.Bind(wx.EVT_TOOL, self.OnNew, id=10)
        
        tb.Realize()        

        
        self.CreateStatusBar()

class RandomSentenceApp(wx.App):
    def OnInit(self):        
        frame = EditorFrame()
        self.SetTopWindow(frame)
        frame.Show(True)
        return True


if __name__ == '__main__':
    app = RandomSentenceApp(0)
    app.MainLoop()