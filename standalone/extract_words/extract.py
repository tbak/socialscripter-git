import sys

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "usage: extract.py input output"
        exit()
        
    lines_in = open(sys.argv[1], 'r')
    
    word_set = set()
    
    for line in lines_in:
        for word in line.split(' '):    
            # print word
            word_set.add(word.strip())
    
    print word_set
    ordered_words = list(word_set)
    ordered_words.sort()
    
    words_out = open(sys.argv[2], 'w')
            
    for word in ordered_words:
        print word
        words_out.write(word + "\n")
            
    