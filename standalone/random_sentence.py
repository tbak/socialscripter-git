from random import shuffle, randrange
import random
import logging
import re
from datetime import date

class Category:
    
    #name = ""
    #replacements = []
    
    def __init__(self, name, line_num, replacement_func=None):
        """ line_num is used so that a category error can
        refer back to the line number in the script file """        
        self.name = name
        self.line_num = line_num
        self.replacements = []
        self.replacement_func = replacement_func
        
    def add_replacement(self, r, prob=1):
        if len(r) == 0:
            return
        
        # Multiply all probabilities by 100 so that we can use
        # floats but that just use randrange()
        self.replacements.append( (r,prob*100) )
        
    def create_sample(self):
        self.add_replacement("hello")
        self.add_replacement("yo",0.5)            
        
    def get_random(self):
        selected_text = None
        current_prob = 0
        
        # Use a function instead?
        if self.replacement_func:
            return self.replacement_func()
        
        for r in self.replacements:
            # TB TODO - Does this work with floats?
            # TB TODO - This is not working right!
            # Now it's working with the *100 fix?
            # print str(current_prob+r[1]) + "<=" + str(r[1])
            if randrange(0,current_prob+r[1]) <= r[1]:
                selected_text = r[0]
                
            current_prob = current_prob + r[1]
          
          
        if selected_text == None:
            print "Nothing was selected?"
            print self.name
            print self.replacements  
            
        # TB TODO - Is there a better way to do this? Do I care?        
        #global debug_buffer
        #debug_buffer = debug_buffer + selected_text
        
        return selected_text

class Lex():

    def __init__(self):
        self.errors = []
        self.warnings = []
        self.cats = {}
        self.debug_enabled = False
        self.debug_buffer = ""
    
    def load_lex(self, filename):
        lex_file = open(filename, 'r')
        self.build_lex(lex_file)
    
    def replacement_func_DAY(self):
        day_of_week = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')
        return day_of_week[date.weekday(date.today())]
        
    def build_lex(self, input):

        cat = None            
        line_num = -1            

        # Add the system categories
        self.cats['_DAY'] = Category("_DAY", 0, self.replacement_func_DAY)
        
        
        #for line in input.readlines():
        for line in input:
            
            line_num = line_num + 1
            
            if len(line.lstrip().rstrip()) == 0:
                continue
            
            if line[0] == '#':
                                
                name = line[1:].lstrip().rstrip()
                
                if name in self.cats:
                    self.errors.append( (line_num, "Category \"%s\" has already been declared on line %d."%(name,self.cats[name].line_num)) )                    
                
                cat = Category(name, line_num)
                self.cats[name] = cat
            
            else:    
            
                if cat == None:
                    self.errors.append( (line_num, "Text has no category associated with it") )
            
                else:             
                
                    # Look for probability token
                    string_end = len(line)
                    probability = 1
                    prob_start = line.find('{')
                    prob_end = line.find('}')
                    #if prob_start != -1 and prob_end == len(line)-1 and prob_start < prob_end:
                    if prob_start != -1:                                                
                        # TB TODO - Extract float from the value
                        try:
                            probability = float(line[prob_start+1:prob_end])
                        except(ValueError):
                            self.errors.append(("Probability value inside of {} is not a valid number.", line_num))
                            
                        string_end = prob_start
                        #logging.debug(probability)               
                        #logging.debug(line[0:string_end])                    
                    
                    cat.add_replacement(line[0:string_end].lstrip().rstrip(), probability)
                
            #print line.lstrip().rstrip()
            
        self.check_lex()           
        
    
    def check_lex(self):
        """ Returns errors,warnings
        Errors and warnings are in the form [(line_num,text),...]
        
        Checks for:
        replacement refers to cat that doesn't exist (error)
        "start" does not exist (error)
        Mismatched <> (error)
        Empty cat (error)
        Two cats with same name(error)
        cat is not used anywhere (warning)         
        """                
        
        # Does start exist?
        if 'start' not in self.cats:
            self.errors.append( (0, "\"start\" category could not be found.") )
        
        # Category has not replacement lines
        for cat in self.cats.values():    
            if len(cat.replacements) == 0 and cat.replacement_func is None:
                self.errors.append( (cat.line_num, "Category \"" + cat.name + "\" does not have any replacement lines (it's empty).") )
                    
            
        # Get a dict with all category names that we are referencing in the text
        # { name : [usage0_line_num, usage1_line_num,etc], ... }
        used_cats = {}
        used_cats['start'] = [0]
        # +? is nongreedy. It'll consume the first <xxx> instead of "<xxx>dsfdsf<xxx>" if regular greedy + were used.
        re_catrefs = re.compile(r'<(.+?)>')
        for cat in self.cats.values():
            for r in cat.replacements:
                # Add all used cats to dict                
                # Iterating over a simple regex match would do this nicely... too bad I have NO INTERNET!!!
                                
                #if re_catrefs.search(r[0]) is not None:
                for match in re_catrefs.findall(r[0]):
                    #used_cats[match] = True
                    used_cats.setdefault(match,[]).append( cat.line_num )
       
        # used_cats = {'start':(5), 'omega':(12,15)}
        
        # Category is not used anywhere?
        for k,v in self.cats.items():
            
            # Skip the system cats that are auto-added to the lex, but don't need to be used
            if k[0] == "_":
                continue
            
            if k not in used_cats:
                # TB TODO - Put this back once used_cats works!!! Right now it just spams with crap
                self.warnings.append( (v.line_num, "Category \"" + v.name + "\" is not used anywhere.") )
                #pass
                
        # Text is referring to a category that doesn't exist
        for k,v in used_cats.items():
            if k not in self.cats:
                for line_num in v:
                    self.errors.append( (line_num, "Nonexisting category \"" + k + "\" is being referenced.") )            
                
        self.warnings.sort()
        self.errors.sort()     
        
    
    def expand_single(self,text):
        
        # TB TODO - Check if it's gotten too long
        
        start = text.find('<')
        end = text.find('>')
        
        if start==-1 or end==-1:
            return (False, text)
        
        cat_name = text[start+1:end] 
        return (True, text[0:start] + self.cats[cat_name].get_random() + text[end+1:] ) 
    
    def expand_text(self,text):
        
        # Loop thru all instances of <> and replace them with new text
        expand_more = True
        while expand_more:
            
            if self.debug_enabled:            
                self.debug_buffer = self.debug_buffer + text + '\n'
                
            expand_more,text = self.expand_single(text)
        
        # return cats['name'].get_random()
        return text
    
    def generate_sentence(self):
        return self.expand_text("<start>")



if __name__ == '__main__':
    
    print 'RANDOM SENTENCE'

    lex = Lex()
    lex.load_lex('c:/p2/socialscripter/media/scripts/test.txt')    
    
    if len(lex.warnings) > 0:
        print "Warnings:"
        for warning in lex.warnings:
            print "Line %d: %s" % (warning[0], warning[1])
            
    if len(lex.errors) > 0:
        print "Errors:"
        for error in lex.errors:
            print "Line %d: %s" % (error[0], error[1])
        exit(-1)
    
    output = lex.generate_sentence()

    if lex.debug_enabled:    
        print "debug:\n" + lex.debug_buffer
    
    print "output:\n" + output
        
    # for i in range(0,20):
    #     print cats['name_actual'].get_random()
    